package com.mini.mybatis.mybatisministep05.sqlsession;

public interface SqlSessionFactory {

    SqlSession openSession();

}
