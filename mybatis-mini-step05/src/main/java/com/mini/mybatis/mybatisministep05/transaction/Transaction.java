package com.mini.mybatis.mybatisministep05.transaction;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * 事务顶层接口, 事务的连接，提交，回滚，关闭
 */
public interface Transaction {

    Connection getConnection() throws SQLException;

    void commit() throws SQLException;

    void rollback() throws SQLException;

    void close() throws SQLException;

}
