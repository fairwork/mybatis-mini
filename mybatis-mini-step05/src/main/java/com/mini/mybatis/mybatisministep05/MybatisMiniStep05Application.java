package com.mini.mybatis.mybatisministep05;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MybatisMiniStep05Application {

    public static void main(String[] args) {
        SpringApplication.run(MybatisMiniStep05Application.class, args);
    }

}
