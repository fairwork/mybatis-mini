package com.mini.mybatis.mybatisministep05.mapper;

import com.mini.mybatis.mybatisministep05.entity.Team;

public interface TeamMapper {


    Team selectByTeamId(Integer teamId);

}
