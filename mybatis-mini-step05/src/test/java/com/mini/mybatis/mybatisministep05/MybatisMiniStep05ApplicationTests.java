package com.mini.mybatis.mybatisministep05;

import com.mini.mybatis.mybatisministep05.datasource.pooled.PooledDataSource;
import com.mini.mybatis.mybatisministep05.entity.Team;
import com.mini.mybatis.mybatisministep05.io.Resources;
import com.mini.mybatis.mybatisministep05.mapper.TeamMapper;
import com.mini.mybatis.mybatisministep05.sqlsession.SqlSession;
import com.mini.mybatis.mybatisministep05.sqlsession.SqlSessionFactory;
import com.mini.mybatis.mybatisministep05.sqlsession.SqlSessionFactoryBuilder;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

@SpringBootTest
class MybatisMiniStep05ApplicationTests {

    @Test
    void contextLoads() {
    }


    @Test
    public void test() throws IOException {
        // 根据配置文件获取 sqlSessionFactory
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(Resources.getResourceAsReader("mybatis-config.xml"));
        SqlSession sqlSession = sqlSessionFactory.openSession();
        TeamMapper teamMapper = sqlSession.getMapper(TeamMapper.class);
        for (int i = 0; i < 50; i++) {
            Team team = teamMapper.selectByTeamId(1005);
            System.out.println(team);
        }
    }

    @Test
    public void test_pooled() throws SQLException, InterruptedException {
        PooledDataSource pooledDataSource = new PooledDataSource();
        pooledDataSource.setDriver("com.mysql.jdbc.Driver");
        pooledDataSource.setUrl("jdbc:mysql://127.0.0.1:3306/nba?useUnicode=true");
        pooledDataSource.setUsername("root");
        pooledDataSource.setPassword("123456");
        // 持续获得链接
        while (true){
            Connection connection = pooledDataSource.getConnection();
            System.out.println(connection);
            Thread.sleep(1000);
            connection.close();
        }
    }

}
