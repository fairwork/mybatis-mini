package com.mini.mybatis.mybatisministep13.mapper;

import com.mini.mybatis.mybatisministep13.annotations.Select;
import com.mini.mybatis.mybatisministep13.entity.Team;

public interface TeamAnnoMapper {

    @Select("select * from team where team_id = #{teamId}")
    Team selectByAnno(Integer teamId);

}
