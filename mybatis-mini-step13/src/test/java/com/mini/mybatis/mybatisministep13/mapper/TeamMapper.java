package com.mini.mybatis.mybatisministep13.mapper;

import com.mini.mybatis.mybatisministep13.entity.Team;

import java.util.List;

public interface TeamMapper {


    Team selectByTeamId(Integer teamId);

    Team selectByTeamName(String teamName);

    List<Team> selectAllTeam();

    void insert(Team team);

    int update(Team team);

    int delete(Integer teamId);



}
