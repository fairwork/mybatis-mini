package com.mini.mybatis.mybatisministep13.sqlsession;

public interface ResultHandler {

    void handleResult(ResultContext context);

}
