package com.mini.mybatis.mybatisministep13.mapping;

public interface SqlSource {

    BoundSql getBoundSql(Object parameterObject);

}
