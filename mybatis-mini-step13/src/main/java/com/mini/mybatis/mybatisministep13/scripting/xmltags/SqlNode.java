package com.mini.mybatis.mybatisministep13.scripting.xmltags;


public interface SqlNode {

    boolean apply(DynamicContext context);

}