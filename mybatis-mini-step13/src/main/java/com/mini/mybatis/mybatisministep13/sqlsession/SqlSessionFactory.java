package com.mini.mybatis.mybatisministep13.sqlsession;

public interface SqlSessionFactory {

    SqlSession openSession();

}
