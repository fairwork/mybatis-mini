package com.mini.mybatis.mybatisministep13.parsing;


public interface TokenHandler {

    String handleToken(String content);

}
