package com.mini.mybatis.mybatisministep13;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MybatisMiniStep13Application {

    public static void main(String[] args) {
        SpringApplication.run(MybatisMiniStep13Application.class, args);
    }

}
