package com.mini.mybatis.mybatisministep13.reflection.invoker;

public interface Invoker {

    Object invoke(Object object, Object[] args) throws Exception;

    Class<?> getType();

}
