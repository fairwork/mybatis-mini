package com.mini.mybatis.mybatisministep13.builder.xml;

import com.mini.mybatis.mybatisministep13.builder.BaseBuilder;
import com.mini.mybatis.mybatisministep13.builder.MapperBuilderAssistant;
import com.mini.mybatis.mybatisministep13.mapping.SqlCommandType;
import com.mini.mybatis.mybatisministep13.mapping.SqlSource;
import com.mini.mybatis.mybatisministep13.scripting.LanguageDriver;
import com.mini.mybatis.mybatisministep13.sqlsession.configuration.Configuration;
import org.dom4j.Element;

import java.util.Locale;

public class XmlStatementBuilder extends BaseBuilder {

    private MapperBuilderAssistant builderAssistant;

    private Element element;


    public XmlStatementBuilder(Configuration configuration, MapperBuilderAssistant builderAssistant, Element element) {
        super(configuration);
        this.builderAssistant = builderAssistant;
        this.element = element;
    }

    // 这里是解析一个sql标签
    public void parse() {
        // id
        String id = element.attributeValue("id");
        // 参数类型
        String parameterType = element.attributeValue("parameterType");
        Class<?> parameterTypeClass = resolveAlias(parameterType);
        // 返回结果类型
        String resultType = element.attributeValue("resultType");
        Class<?> resultTypeClass = resolveAlias(resultType);
        // resultMap
        String resultMap = element.attributeValue("resultMap");
        // sql命令类型
        String name = element.getName();
        SqlCommandType sqlCommandType = SqlCommandType.valueOf(name.toUpperCase(Locale.ENGLISH));

        // 获取语言驱动器 这里暂时获取的是默认的xmlLanguageDriver,后续可通过配置获取对应类型的driver
        Class<?> langClass = configuration.getLanguageRegistry().getDefaultDriverClass();
        LanguageDriver langDriver = configuration.getLanguageRegistry().getDriver(langClass);

        // 通过语言驱动器来解析sql并封装为sqlSource，这里可以根据类型解析动态sql和静态sql
        SqlSource sqlSource = langDriver.createSqlSource(configuration, element, parameterTypeClass);

        // 解耦，引入助手类，添加映射，不让单个类承担过多的功能
        builderAssistant.addMappedStatement(id,
                sqlSource,
                sqlCommandType,
                parameterTypeClass,
                resultMap,
                resultTypeClass,
                langDriver);
    }

}
