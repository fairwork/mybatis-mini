package com.mini.mybatis.mybatisministep06.executor.statement;

import com.mini.mybatis.mybatisministep06.sqlsession.ResultHandler;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

/**
 * mybatis四大组件之一StatementHandler，主要处理sql语句相关的操作
 */
public interface StatementHandler {


    /**
     * 准备语句
     * @param connection
     * @return
     * @throws SQLException
     */
    Statement prepare(Connection connection) throws SQLException;


    /**
     * 参数化
     * @param statement
     * @throws SQLException
     */
    void parameterize(Statement statement) throws SQLException;


    /**
     * 执行sql查询
     * @param statement
     * @param resultHandler
     * @return
     * @param <E>
     * @throws SQLException
     */
    <E> List<E> query(Statement statement, ResultHandler resultHandler) throws SQLException;

}
