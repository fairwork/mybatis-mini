package com.mini.mybatis.mybatisministep06.sqlsession;

public interface SqlSessionFactory {

    SqlSession openSession();

}
