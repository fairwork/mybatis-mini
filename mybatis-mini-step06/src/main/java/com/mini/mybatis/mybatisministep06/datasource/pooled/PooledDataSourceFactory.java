package com.mini.mybatis.mybatisministep06.datasource.pooled;

import com.mini.mybatis.mybatisministep06.datasource.unpooled.UnPooledDataSourceFactory;

import javax.sql.DataSource;

public class PooledDataSourceFactory extends UnPooledDataSourceFactory {

    @Override
    public DataSource getDataSource() {
        PooledDataSource pooledDataSource = new PooledDataSource();
        pooledDataSource.setDriver(properties.getProperty("driver"));
        pooledDataSource.setUrl(properties.getProperty("url"));
        pooledDataSource.setUsername(properties.getProperty("username"));
        pooledDataSource.setPassword(properties.getProperty("password"));
        return pooledDataSource;
    }
}
