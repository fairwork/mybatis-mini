package com.mini.mybatis.mybatisministep06.executor.resultset;

import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

/**
 * mybatis四大组件之一ResultSetHandler，主要是对结果集的处理
 */
public interface ResultSetHandler {

    <E> List<E> handleResultSets(Statement statement) throws SQLException;

}
