package com.mini.mybatis.mybatisministep06.sqlsession;

import com.mini.mybatis.mybatisministep06.sqlsession.configuration.Configuration;

public interface SqlSession {


    /**
     * 根据指定的sqlId获取封装对象
     * @param statement
     * @return
     * @param <T>
     */
    <T> T selectOne(String statement);



    /**
     * 根据指定的sqlId和参数获取封装对象
     * @param statement
     * @param parameter
     * @return
     * @param <T>
     */
    <T> T selectOne(String statement,Object parameter);



    /**
     * 根据mapper类Class获取对应类型MapperProxyFactory
     * @param type
     * @return
     * @param <T>
     */
    <T> T getMapper(Class<T> type);


    Configuration getConfiguration();

}
