package com.mini.mybatis.mybatisministep06.executor;

import com.mini.mybatis.mybatisministep06.executor.statement.StatementHandler;
import com.mini.mybatis.mybatisministep06.mapping.BoundSql;
import com.mini.mybatis.mybatisministep06.mapping.MappedStatement;
import com.mini.mybatis.mybatisministep06.sqlsession.ResultHandler;
import com.mini.mybatis.mybatisministep06.sqlsession.configuration.Configuration;
import com.mini.mybatis.mybatisministep06.transaction.Transaction;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

public class SimpleExecutor extends BaseExecutor{

    public SimpleExecutor(Configuration configuration, Transaction transaction) {
        super(configuration, transaction);
    }

    @Override
    protected <E> List<E> doQuery(MappedStatement ms, Object parameter, ResultHandler resultHandler, BoundSql boundSql) {
        try {
            Configuration configuration = ms.getConfiguration();
            StatementHandler statementHandler = configuration.newStatementHandler(this,ms,parameter,resultHandler,boundSql);
            Connection connection = transaction.getConnection();
            Statement statement = statementHandler.prepare(connection);
            statementHandler.parameterize(statement);
            return statementHandler.query(statement,resultHandler);
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }
}
