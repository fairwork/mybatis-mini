package com.mini.mybatis.mybatisministep06;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MybatisMiniStep06Application {

    public static void main(String[] args) {
        SpringApplication.run(MybatisMiniStep06Application.class, args);
    }

}
