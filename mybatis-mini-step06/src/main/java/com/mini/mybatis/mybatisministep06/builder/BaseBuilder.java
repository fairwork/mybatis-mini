package com.mini.mybatis.mybatisministep06.builder;

import com.mini.mybatis.mybatisministep06.sqlsession.configuration.Configuration;
import com.mini.mybatis.mybatisministep06.type.TypeAliasRegistry;

public class BaseBuilder {

    protected Configuration configuration;

    protected TypeAliasRegistry typeAliasRegistry;

    public BaseBuilder(Configuration configuration) {
        this.configuration = configuration;
        typeAliasRegistry = this.configuration.getTypeAliasRegistry();
    }

    public Configuration getConfiguration(){
        return configuration;
    }
}
