package com.mini.mybatis.mybatisministep06.executor;

import com.mini.mybatis.mybatisministep06.mapping.BoundSql;
import com.mini.mybatis.mybatisministep06.mapping.MappedStatement;
import com.mini.mybatis.mybatisministep06.sqlsession.ResultHandler;
import com.mini.mybatis.mybatisministep06.transaction.Transaction;

import java.sql.SQLException;
import java.util.List;

/**
 * mybatis四大组件之一Executor执行器，主要处理执行相关的操作
 */
public interface Executor {

    ResultHandler NO_RESULT_HANDLER = null;

    <E> List<E> query(MappedStatement ms, Object parameter, ResultHandler resultHandler, BoundSql boundSql);

    Transaction getTransaction();

    void commit(boolean require) throws SQLException;

    void rollback(boolean require) throws SQLException;

    void close(boolean forceRollback);

}
