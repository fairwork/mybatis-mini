package com.mini.mybatis.mybatisministep06;

import com.mini.mybatis.mybatisministep06.datasource.pooled.PooledDataSource;
import com.mini.mybatis.mybatisministep06.entity.Team;
import com.mini.mybatis.mybatisministep06.io.Resources;
import com.mini.mybatis.mybatisministep06.mapper.TeamMapper;
import com.mini.mybatis.mybatisministep06.sqlsession.SqlSession;
import com.mini.mybatis.mybatisministep06.sqlsession.SqlSessionFactory;
import com.mini.mybatis.mybatisministep06.sqlsession.SqlSessionFactoryBuilder;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

@SpringBootTest
class MybatisMiniStep06ApplicationTests {

    @Test
    void contextLoads() {
    }



    @Test
    public void test() throws IOException {
        // 首先通过配置文件获取sqlSessionFactory
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(Resources.getResourceAsReader("mybatis-config.xml"));
        // 获取sqlSession
        SqlSession sqlSession = sqlSessionFactory.openSession();
        // 获取指定Mapper的代理
        TeamMapper teamMapper = sqlSession.getMapper(TeamMapper.class);
        // 执行代理对象的查询方法
        Team team = teamMapper.selectByTeamId(1006);
        System.out.println(team);
    }
}
