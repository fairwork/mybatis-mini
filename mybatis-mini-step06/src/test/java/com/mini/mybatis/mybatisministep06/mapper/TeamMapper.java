package com.mini.mybatis.mybatisministep06.mapper;

import com.mini.mybatis.mybatisministep06.entity.Team;

public interface TeamMapper {


    Team selectByTeamId(Integer teamId);

}
