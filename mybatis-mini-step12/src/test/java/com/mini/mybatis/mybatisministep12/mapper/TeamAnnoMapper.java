package com.mini.mybatis.mybatisministep12.mapper;

import com.mini.mybatis.mybatisministep12.annotations.Select;
import com.mini.mybatis.mybatisministep12.entity.Team;

public interface TeamAnnoMapper {

    @Select("select * from team where team_id = #{teamId}")
    Team selectByAnno(Integer teamId);

}
