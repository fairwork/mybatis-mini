package com.mini.mybatis.mybatisministep12.parsing;


public interface TokenHandler {

    String handleToken(String content);

}
