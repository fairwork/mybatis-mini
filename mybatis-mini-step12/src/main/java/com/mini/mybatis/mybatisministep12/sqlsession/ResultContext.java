package com.mini.mybatis.mybatisministep12.sqlsession;


public interface ResultContext {

    /**
     * 获取结果
     */
    Object getResultObject();

    /**
     * 获取记录数
     */
    int getResultCount();

}
