package com.mini.mybatis.mybatisministep12.mapping;

public interface SqlSource {

    BoundSql getBoundSql(Object parameterObject);

}
