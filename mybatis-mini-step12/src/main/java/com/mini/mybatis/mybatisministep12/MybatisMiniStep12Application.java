package com.mini.mybatis.mybatisministep12;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MybatisMiniStep12Application {

    public static void main(String[] args) {
        SpringApplication.run(MybatisMiniStep12Application.class, args);
    }

}
