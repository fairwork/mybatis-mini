package com.mini.mybatis.mybatisministep12.sqlsession;

public interface ResultHandler {

    void handleResult(ResultContext context);

}
