package com.mini.mybatis.mybatisministep12.scripting.xmltags;


import com.mini.mybatis.mybatisministep12.executor.parameter.ParameterHandler;
import com.mini.mybatis.mybatisministep12.mapping.BoundSql;
import com.mini.mybatis.mybatisministep12.mapping.MappedStatement;
import com.mini.mybatis.mybatisministep12.mapping.SqlSource;
import com.mini.mybatis.mybatisministep12.scripting.LanguageDriver;
import com.mini.mybatis.mybatisministep12.scripting.defaults.DefaultParameterHandler;
import com.mini.mybatis.mybatisministep12.scripting.defaults.RawSqlSource;
import com.mini.mybatis.mybatisministep12.sqlsession.configuration.Configuration;
import org.dom4j.Element;


public class XMLLanguageDriver implements LanguageDriver {

    @Override
    public SqlSource createSqlSource(Configuration configuration, Element script, Class<?> parameterType) {
        // 用XML脚本构建器解析sql
        XMLScriptBuilder builder = new XMLScriptBuilder(configuration, script, parameterType);
        return builder.parseScriptNode();
    }

    /**
     * 用于处理注解配置 SQL 语句
     */
    @Override
    public SqlSource createSqlSource(Configuration configuration, String script, Class<?> parameterType) {
        // 暂时不解析动态 SQL
        return new RawSqlSource(configuration, script, parameterType);
    }

    @Override
    public ParameterHandler createParameterHandler(MappedStatement mappedStatement, Object parameterObject, BoundSql boundSql) {
        return new DefaultParameterHandler(mappedStatement, parameterObject, boundSql);
    }

}