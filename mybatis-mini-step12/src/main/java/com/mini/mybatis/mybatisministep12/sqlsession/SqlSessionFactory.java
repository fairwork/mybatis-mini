package com.mini.mybatis.mybatisministep12.sqlsession;

public interface SqlSessionFactory {

    SqlSession openSession();

}
