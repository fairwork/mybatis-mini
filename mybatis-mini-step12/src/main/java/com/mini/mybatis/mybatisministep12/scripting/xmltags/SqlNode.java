package com.mini.mybatis.mybatisministep12.scripting.xmltags;


public interface SqlNode {

    boolean apply(DynamicContext context);

}