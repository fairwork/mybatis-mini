package com.mini.mybatis.mybatisministep07.builder.xml;

import com.mini.mybatis.mybatisministep07.builder.BaseBuilder;
import com.mini.mybatis.mybatisministep07.datasource.DataSourceFactory;
import com.mini.mybatis.mybatisministep07.io.Resources;
import com.mini.mybatis.mybatisministep07.mapping.BoundSql;
import com.mini.mybatis.mybatisministep07.mapping.Environment;
import com.mini.mybatis.mybatisministep07.mapping.MappedStatement;
import com.mini.mybatis.mybatisministep07.mapping.SqlCommandType;
import com.mini.mybatis.mybatisministep07.sqlsession.configuration.Configuration;
import com.mini.mybatis.mybatisministep07.transaction.TransactionFactory;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.xml.sax.InputSource;

import javax.sql.DataSource;
import java.io.Reader;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class XmlConfigBuilder extends BaseBuilder {

    private Element rootElement;

    public XmlConfigBuilder(Reader reader) {

        // 调用父类初始化configuration
        super(new Configuration());

        // 利用dom4j解析xml
        SAXReader saxReader = new SAXReader();

        try {
            Document document = saxReader.read(new InputSource(reader));
            rootElement = document.getRootElement();
        } catch (DocumentException e) {
            throw new RuntimeException(e);
        }
    }


    /**
     * 解析mybatis全局配置文件,主要解析mappers
     * @return
     */
    public Configuration parse(){

        try {
            // 解析environments
            environmentsElement(rootElement.element("environments"));

            // 解析mappers
            mappersElement(rootElement.element("mappers"));

        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return configuration;
    }

    private void environmentsElement(Element environments) throws Exception {
        // 解析environments标签
        String environmentId = environments.attributeValue("default");
        List<Element> elementList = environments.elements("environment");
        for (Element e : elementList) {
            // 每个environment标签的id
            String id = e.attributeValue("id");
            if (environmentId.equals(id)){
                // 事务管理器 通过配置类中的type类型，获取已经在Configuration类中初始化时保存好的事务管理器，下面的DataSourceFactory也一样
                TransactionFactory txFactory = (TransactionFactory) typeAliasRegistry.resolveAlias(e.element("transactionManager").attributeValue("type")).newInstance();
                // 数据源
                Element datasourceElement = e.element("dataSource");
                DataSourceFactory dataSourceFactory = (DataSourceFactory) typeAliasRegistry.resolveAlias(datasourceElement.attributeValue("type")).newInstance();
                List<Element> propertyList = datasourceElement.elements("property");
                Properties props = new Properties();
                for (Element property : propertyList) {
                    props.setProperty(property.attributeValue("name"),property.attributeValue("value"));
                }
                dataSourceFactory.setProperties(props);
                DataSource dataSource = dataSourceFactory.getDataSource();

                Environment.Builder environmentBuilder = new Environment.Builder(id)
                        .transactionFactory(txFactory)
                        .dataSource(dataSource);

                configuration.setEnvironment(environmentBuilder.build());
            }
        }
    }

    /**
     * <mappers>
     *      <mapper resource="mapper/UserMapper.xml"/>
     *      <mapper resource="mapper/PetMapper.xml"/>
     *  </mappers>
     *
     * 读取每个mapper.xml并解析，
     * @param mappers
     * @throws Exception
     */
    private void mappersElement(Element mappers) throws Exception{

        List<Element> mapperList = mappers.elements("mapper");

        for (Element e : mapperList) {
            String resource = e.attributeValue("resource");
            Reader reader = Resources.getResourceAsReader(resource);
            SAXReader saxReader = new SAXReader();
            Document document = saxReader.read(reader);
            Element root = document.getRootElement();

            // 命名空间
            String namespace = root.attributeValue("namespace");

            // 解析select标签
            List<Element> selectNodes = root.elements("select");
            for (Element selectNode : selectNodes) {
                String id = selectNode.attributeValue("id");
                String parameterType = selectNode.attributeValue("parameterType");
                String resultType = selectNode.attributeValue("resultType");
                String sql = selectNode.getText();

                // ? 匹配参数 #{type}
                Map<Integer,String>  parameter = new HashMap<>();
                Pattern pattern = Pattern.compile("(#\\{(.*?)})");
                Matcher matcher = pattern.matcher(sql);
                for (int i = 1; matcher.find(); i++) {
                    String g1 = matcher.group(1); // #{type}
                    String g2 = matcher.group(2); // type
                    parameter.put(i, g2); // parameter.put(1, type);
                    sql = sql.replace(g1, "?");
                }

                String msId = namespace + "." + id;
                String nodeName = selectNode.getName();
                SqlCommandType sqlCommandType = SqlCommandType.valueOf(nodeName.toUpperCase(Locale.ENGLISH));
                BoundSql boundSql = new BoundSql(sql,parameter,parameterType,resultType);

                MappedStatement mappedStatement = new MappedStatement.Builder(configuration, msId, sqlCommandType, boundSql).build();
                configuration.addMappedStatement(mappedStatement);
            }

            // 注册mapper映射器
            configuration.addMapper(Resources.classForName(namespace));

        }
    }
}
