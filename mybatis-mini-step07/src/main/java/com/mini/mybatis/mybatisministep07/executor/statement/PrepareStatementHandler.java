package com.mini.mybatis.mybatisministep07.executor.statement;

import com.mini.mybatis.mybatisministep07.executor.Executor;
import com.mini.mybatis.mybatisministep07.mapping.BoundSql;
import com.mini.mybatis.mybatisministep07.mapping.MappedStatement;
import com.mini.mybatis.mybatisministep07.sqlsession.ResultHandler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

public class PrepareStatementHandler extends BaseStatementHandler{


    public PrepareStatementHandler(Executor executor, MappedStatement mappedStatement, Object parameterObject, ResultHandler resultHandler, BoundSql boundSql) {
        super(executor, mappedStatement, parameterObject, resultHandler, boundSql);
    }

    @Override
    protected Statement instantiateStatement(Connection connection) throws SQLException {
        String sql = boundSql.getSql();
        return connection.prepareStatement(sql);
    }

    @Override
    public void parameterize(Statement statement) throws SQLException {
        PreparedStatement ps = (PreparedStatement) statement;
        ps.setInt(1,Integer.parseInt(((Object[])parameterObject)[0].toString()));
    }

    @Override
    public <E> List<E> query(Statement statement, ResultHandler resultHandler) throws SQLException {
        PreparedStatement ps = (PreparedStatement) statement;
        ps.execute();
        return resultSetHandler.handleResultSets(ps);
    }
}
