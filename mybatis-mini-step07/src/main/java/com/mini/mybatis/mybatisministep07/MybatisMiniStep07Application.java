package com.mini.mybatis.mybatisministep07;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MybatisMiniStep07Application {

    public static void main(String[] args) {
        SpringApplication.run(MybatisMiniStep07Application.class, args);
    }

}
