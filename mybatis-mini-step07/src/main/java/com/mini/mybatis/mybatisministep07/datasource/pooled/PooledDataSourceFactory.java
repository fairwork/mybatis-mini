package com.mini.mybatis.mybatisministep07.datasource.pooled;

import com.mini.mybatis.mybatisministep07.datasource.unpooled.UnPooledDataSourceFactory;

import javax.sql.DataSource;

public class PooledDataSourceFactory extends UnPooledDataSourceFactory {

    public PooledDataSourceFactory(){
        this.dataSource = new PooledDataSource();
    }

}
