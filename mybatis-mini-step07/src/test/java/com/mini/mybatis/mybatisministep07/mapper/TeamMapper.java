package com.mini.mybatis.mybatisministep07.mapper;

import com.mini.mybatis.mybatisministep07.entity.Team;

public interface TeamMapper {


    Team selectByTeamId(Integer teamId);

}
