package com.mini.mybatis.mybatisministep07;

import com.mini.mybatis.mybatisministep07.entity.Team;
import com.mini.mybatis.mybatisministep07.io.Resources;
import com.mini.mybatis.mybatisministep07.mapper.TeamMapper;
import com.mini.mybatis.mybatisministep07.reflection.MetaObject;
import com.mini.mybatis.mybatisministep07.reflection.SystemMetaObject;
import com.mini.mybatis.mybatisministep07.sqlsession.SqlSession;
import com.mini.mybatis.mybatisministep07.sqlsession.SqlSessionFactory;
import com.mini.mybatis.mybatisministep07.sqlsession.SqlSessionFactoryBuilder;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;
import java.util.Arrays;

@SpringBootTest
class MybatisMiniStep07ApplicationTests {

    @Test
    void contextLoads() {
    }



    @Test
    public void test() throws IOException {
        // 首先通过配置文件获取sqlSessionFactory
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(Resources.getResourceAsReader("mybatis-config.xml"));
        // 获取sqlSession
        SqlSession sqlSession = sqlSessionFactory.openSession();
        // 获取指定Mapper的代理
        TeamMapper teamMapper = sqlSession.getMapper(TeamMapper.class);
        // 执行代理对象的查询方法
        Team team = teamMapper.selectByTeamId(1006);
        System.out.println(team);
    }

    @Test
    void testReflection(){
        Team team = new Team();
        MetaObject metaObject = SystemMetaObject.forObject(team);
        String[] getterNames = metaObject.getGetterNames();
        System.out.println(Arrays.toString(getterNames));
        String[] setterNames = metaObject.getSetterNames();
        System.out.println(Arrays.toString(setterNames));
        metaObject.setValue("team_type",55);
        System.out.println(team);

    }
}
