package com.mini.mybatis.mybatisministep07.entity;

public class Team {

    private Integer team_id;

    private String team_name;

    private Integer team_type;

    public Integer getTeam_id() {
        return team_id;
    }

    public void setTeam_id(Integer team_id) {
        this.team_id = team_id;
    }

    public String getTeam_name() {
        return team_name;
    }

    public void setTeam_name(String team_name) {
        this.team_name = team_name;
    }

    public Integer getTeam_type() {
        return team_type;
    }

    public void setTeam_type(Integer team_type) {
        this.team_type = team_type;
    }

    @Override
    public String toString() {
        return "Team{" +
                "team_id=" + team_id +
                ", team_name='" + team_name + '\'' +
                ", team_type=" + team_type +
                '}';
    }
}
