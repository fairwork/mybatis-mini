package com.mini.mybatis.mybatisministep14.mapper;

import com.mini.mybatis.mybatisministep14.annotations.Select;
import com.mini.mybatis.mybatisministep14.entity.Team;

public interface TeamAnnoMapper {

    @Select("select * from team where team_id = #{teamId}")
    Team selectByAnno(Integer teamId);

}
