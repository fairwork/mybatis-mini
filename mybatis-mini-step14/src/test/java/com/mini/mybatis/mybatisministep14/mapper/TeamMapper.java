package com.mini.mybatis.mybatisministep14.mapper;

import com.mini.mybatis.mybatisministep14.entity.Team;

import java.util.List;

public interface TeamMapper {


    Team selectByTeamId(Integer teamId);

    Team selectByTeamName(String teamName);

    List<Team> selectAllTeam();

    void insert(Team team);

    int update(Team team);

    int delete(Integer teamId);



}
