package com.mini.mybatis.mybatisministep14.mapping;

public interface SqlSource {

    BoundSql getBoundSql(Object parameterObject);

}
