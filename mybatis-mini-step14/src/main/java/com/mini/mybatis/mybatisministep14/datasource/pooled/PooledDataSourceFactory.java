package com.mini.mybatis.mybatisministep14.datasource.pooled;

import com.mini.mybatis.mybatisministep14.datasource.unpooled.UnPooledDataSourceFactory;

public class PooledDataSourceFactory extends UnPooledDataSourceFactory {

    public PooledDataSourceFactory(){
        this.dataSource = new PooledDataSource();
    }

}
