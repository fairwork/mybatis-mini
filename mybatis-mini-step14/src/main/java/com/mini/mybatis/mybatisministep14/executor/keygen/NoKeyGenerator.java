package com.mini.mybatis.mybatisministep14.executor.keygen;



import com.mini.mybatis.mybatisministep14.executor.Executor;
import com.mini.mybatis.mybatisministep14.mapping.MappedStatement;

import java.sql.Statement;


public class NoKeyGenerator implements KeyGenerator{

    @Override
    public void processBefore(Executor executor, MappedStatement ms, Statement stmt, Object parameter) {
        // Do Nothing
    }

    @Override
    public void processAfter(Executor executor, MappedStatement ms, Statement stmt, Object parameter) {
        // Do Nothing
    }

}
