package com.mini.mybatis.mybatisministep14.sqlsession;

public interface ResultHandler {

    void handleResult(ResultContext context);

}
