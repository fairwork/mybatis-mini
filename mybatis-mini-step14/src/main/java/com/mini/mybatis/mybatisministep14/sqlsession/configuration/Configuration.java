package com.mini.mybatis.mybatisministep14.sqlsession.configuration;

import com.mini.mybatis.mybatisministep14.binding.MapperRegistry;
import com.mini.mybatis.mybatisministep14.datasource.durid.DruidDataSourceFactory;
import com.mini.mybatis.mybatisministep14.datasource.pooled.PooledDataSourceFactory;
import com.mini.mybatis.mybatisministep14.datasource.unpooled.UnPooledDataSourceFactory;
import com.mini.mybatis.mybatisministep14.executor.Executor;
import com.mini.mybatis.mybatisministep14.executor.SimpleExecutor;
import com.mini.mybatis.mybatisministep14.executor.keygen.KeyGenerator;
import com.mini.mybatis.mybatisministep14.executor.parameter.ParameterHandler;
import com.mini.mybatis.mybatisministep14.executor.resultset.DefaultResultSetHandler;
import com.mini.mybatis.mybatisministep14.executor.resultset.ResultSetHandler;
import com.mini.mybatis.mybatisministep14.executor.statement.PrepareStatementHandler;
import com.mini.mybatis.mybatisministep14.executor.statement.StatementHandler;
import com.mini.mybatis.mybatisministep14.mapping.BoundSql;
import com.mini.mybatis.mybatisministep14.mapping.Environment;
import com.mini.mybatis.mybatisministep14.mapping.MappedStatement;
import com.mini.mybatis.mybatisministep14.mapping.ResultMap;
import com.mini.mybatis.mybatisministep14.reflection.MetaObject;
import com.mini.mybatis.mybatisministep14.reflection.factory.DefaultObjectFactory;
import com.mini.mybatis.mybatisministep14.reflection.factory.ObjectFactory;
import com.mini.mybatis.mybatisministep14.reflection.wrapper.DefaultObjectWrapperFactory;
import com.mini.mybatis.mybatisministep14.reflection.wrapper.ObjectWrapperFactory;
import com.mini.mybatis.mybatisministep14.scripting.LanguageDriver;
import com.mini.mybatis.mybatisministep14.scripting.LanguageDriverRegistry;
import com.mini.mybatis.mybatisministep14.scripting.xmltags.XMLLanguageDriver;
import com.mini.mybatis.mybatisministep14.sqlsession.ResultHandler;
import com.mini.mybatis.mybatisministep14.sqlsession.RowBounds;
import com.mini.mybatis.mybatisministep14.sqlsession.SqlSession;
import com.mini.mybatis.mybatisministep14.transaction.Transaction;
import com.mini.mybatis.mybatisministep14.transaction.jdbc.JdbcTransactionFactory;
import com.mini.mybatis.mybatisministep14.type.TypeAliasRegistry;
import com.mini.mybatis.mybatisministep14.type.TypeHandlerRegistry;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * MyBatis底层全局配置
 */
public class Configuration {

    protected Environment environment;

    protected boolean useGeneratedKeys = false;

    // 映射注册机
    protected MapperRegistry mapperRegistry = new MapperRegistry(this);

    // 映射的语句，存在Map里
    protected final Map<String, MappedStatement> mappedStatements = new HashMap<>();
    // 结果映射，存在Map里
    protected final Map<String, ResultMap> resultMaps = new HashMap<>();

    protected final Map<String, KeyGenerator> keyGenerators = new HashMap<>();


    // 类型别名注册机
    protected final TypeAliasRegistry typeAliasRegistry = new TypeAliasRegistry();
    protected final LanguageDriverRegistry languageRegistry = new LanguageDriverRegistry();

    // 类型处理器注册机
    protected final TypeHandlerRegistry typeHandlerRegistry = new TypeHandlerRegistry();

    // 对象工厂和对象包装器工厂
    protected ObjectFactory objectFactory = new DefaultObjectFactory();
    protected ObjectWrapperFactory objectWrapperFactory = new DefaultObjectWrapperFactory();

    protected final Set<String> loadedResources = new HashSet<>();

    protected String databaseId;

    public Configuration() {
        typeAliasRegistry.registerAlias("JDBC", JdbcTransactionFactory.class);

        typeAliasRegistry.registerAlias("DRUID", DruidDataSourceFactory.class);
        typeAliasRegistry.registerAlias("UNPOOLED", UnPooledDataSourceFactory.class);
        typeAliasRegistry.registerAlias("POOLED", PooledDataSourceFactory.class);

        languageRegistry.setDefaultDriverClass(XMLLanguageDriver.class);
    }

    public void addMappers(String packageName) {
        mapperRegistry.addMappers(packageName);
    }

    public <T> void addMapper(Class<T> type) {
        mapperRegistry.addMapper(type);
    }

    public <T> T getMapper(Class<T> type, SqlSession sqlSession) {
        return mapperRegistry.getMapper(type, sqlSession);
    }

    public boolean hasMapper(Class<?> type) {
        return mapperRegistry.hasMapper(type);
    }

    public void addMappedStatement(MappedStatement ms) {
        mappedStatements.put(ms.getId(), ms);
    }

    public MappedStatement getMappedStatement(String id) {
        return mappedStatements.get(id);
    }

    public TypeAliasRegistry getTypeAliasRegistry() {
        return typeAliasRegistry;
    }

    public Environment getEnvironment() {
        return environment;
    }

    public void setEnvironment(Environment environment) {
        this.environment = environment;
    }

    public String getDatabaseId() {
        return databaseId;
    }

    /**
     * 创建结果集处理器
     */
    public ResultSetHandler newResultSetHandler(Executor executor, MappedStatement mappedStatement, RowBounds rowBounds, ResultHandler resultHandler, BoundSql boundSql) {
        return new DefaultResultSetHandler(executor, mappedStatement, resultHandler, rowBounds, boundSql);
    }

    /**
     * 生产执行器
     */
    public Executor newExecutor(Transaction transaction) {
        return new SimpleExecutor(this, transaction);
    }

    /**
     * 创建语句处理器
     */
    public StatementHandler newStatementHandler(Executor executor, MappedStatement mappedStatement, Object parameter, RowBounds rowBounds, ResultHandler resultHandler, BoundSql boundSql) {
        return new PrepareStatementHandler(executor, mappedStatement, parameter, rowBounds,resultHandler, boundSql);
    }

    // 创建元对象
    public MetaObject newMetaObject(Object object) {
        return MetaObject.forObject(object, objectFactory, objectWrapperFactory);
    }

    // 类型处理器注册机
    public TypeHandlerRegistry getTypeHandlerRegistry() {
        return typeHandlerRegistry;
    }

    public boolean isResourceLoaded(String resource) {
        return loadedResources.contains(resource);
    }

    public void addLoadedResource(String resource) {
        loadedResources.add(resource);
    }

    public LanguageDriverRegistry getLanguageRegistry() {
        return languageRegistry;
    }

    public ParameterHandler newParameterHandler(MappedStatement mappedStatement, Object parameterObject, BoundSql boundSql) {
        // 创建参数处理器
        ParameterHandler parameterHandler = mappedStatement.getLang().createParameterHandler(mappedStatement, parameterObject, boundSql);
        // 插件的一些参数，也是在这里处理，interceptorChain.pluginAll(parameterHandler);
        return parameterHandler;
    }

    public LanguageDriver getDefaultScriptingLanguageInstance() {
        return languageRegistry.getDefaultDriver();
    }

    public ObjectFactory getObjectFactory() {
        return objectFactory;
    }

    public ResultMap getResultMap(String id) {
        return resultMaps.get(id);
    }
    public void addResultMap(ResultMap resultMap) {
        resultMaps.put(resultMap.getId(), resultMap);
    }

    public void addKeyGenerator(String id, KeyGenerator keyGenerator) {
        keyGenerators.put(id, keyGenerator);
    }

    public KeyGenerator getKeyGenerator(String id) {
        return keyGenerators.get(id);
    }

    public boolean hasKeyGenerator(String id) {
        return keyGenerators.containsKey(id);
    }

    public boolean isUseGeneratedKeys() {
        return useGeneratedKeys;
    }

    public void setUseGeneratedKeys(boolean useGeneratedKeys) {
        this.useGeneratedKeys = useGeneratedKeys;
    }

}
