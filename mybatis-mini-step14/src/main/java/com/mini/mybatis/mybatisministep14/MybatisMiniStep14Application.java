package com.mini.mybatis.mybatisministep14;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MybatisMiniStep14Application {

    public static void main(String[] args) {
        SpringApplication.run(MybatisMiniStep14Application.class, args);
    }

}
