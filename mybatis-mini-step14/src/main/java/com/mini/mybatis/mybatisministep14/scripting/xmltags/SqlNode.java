package com.mini.mybatis.mybatisministep14.scripting.xmltags;


public interface SqlNode {

    boolean apply(DynamicContext context);

}