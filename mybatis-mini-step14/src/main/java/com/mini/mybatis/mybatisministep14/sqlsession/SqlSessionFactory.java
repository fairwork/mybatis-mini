package com.mini.mybatis.mybatisministep14.sqlsession;

public interface SqlSessionFactory {

    SqlSession openSession();

}
