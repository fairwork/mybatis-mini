package com.mini.mybatis.mybatisministep03.mapping;

public enum SqlCommandType {

    /**
     * 未知
     */
    UNKNOWN,

    /**
     * 插入
     */
    INSERT,

    /**
     * 删除
     */
    DELETE,

    /**
     * 更新
     */
    UPDATE,

    /**
     * 查询
     */
    SELECT



}
