package com.mini.mybatis.mybatisministep03.binding;

import com.mini.mybatis.mybatisministep03.mapping.MappedStatement;
import com.mini.mybatis.mybatisministep03.mapping.SqlCommandType;
import com.mini.mybatis.mybatisministep03.sqlsession.SqlSession;
import com.mini.mybatis.mybatisministep03.sqlsession.configuration.Configuration;

import java.lang.reflect.Method;

public class MapperMethod {

    private SqlCommand sqlCommand;

    public MapperMethod(Configuration configuration, Class<?> mapperInterface, Method method){
        this.sqlCommand = new SqlCommand(configuration,mapperInterface,method);
    }

    public Object execute(SqlSession sqlSession, Object[] args) {
        Object result = null;
        switch (sqlCommand.getType()){
            case UNKNOWN:
                break;
            case INSERT:
                break;
            case DELETE:
                break;
            case UPDATE:
                break;
            case SELECT:
                result = sqlSession.selectOne(sqlCommand.getId(),args);
                break;
            default:
                throw new RuntimeException("unable process sqlCommand: "+sqlCommand.id);
        }
        return result;
    }


    public static class SqlCommand{

        private String id;
        private SqlCommandType type;

        public SqlCommand(Configuration configuration, Class<?> mapperInterface, Method method){
            String statement = mapperInterface.getName() + "." + method.getName();
            MappedStatement mapStatement = configuration.getMapStatement(statement);
            this.id = mapStatement.getId();
            this.type = mapStatement.getSqlCommandType();
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public SqlCommandType getType() {
            return type;
        }

        public void setType(SqlCommandType type) {
            this.type = type;
        }
    }

}
