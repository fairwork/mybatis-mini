package com.mini.mybatis.mybatisministep03.sqlsession.configuration;

import com.mini.mybatis.mybatisministep03.binding.MapperRegistry;
import com.mini.mybatis.mybatisministep03.mapping.MappedStatement;
import com.mini.mybatis.mybatisministep03.sqlsession.SqlSession;

import java.util.HashMap;
import java.util.Map;

/**
 * MyBatis底层全局配置
 */
public class Configuration {


    // 映射注册机
    protected MapperRegistry mapperRegistry = new MapperRegistry(this);

    // 映射语句
    protected Map<String, MappedStatement> mappedStatements = new HashMap<>();

    public <T> void addMapper(Class<T> type){
        this.mapperRegistry.addMapper(type);
    }

    public void addMappers(String packName){
        this.mapperRegistry.addMappers(packName);
    }

    public <T> T getMapper(Class<T> type, SqlSession sqlSession){
        return this.mapperRegistry.getMapper(type,sqlSession);
    }

    public <T> boolean hasMapper(Class<T> type){
        return this.mapperRegistry.hasMapper(type);
    }

    public void addMappedStatement(MappedStatement ms){
        this.mappedStatements.put(ms.getId(),ms);
    }

    public MappedStatement getMapStatement(String id){
        return this.mappedStatements.get(id);
    }




}
