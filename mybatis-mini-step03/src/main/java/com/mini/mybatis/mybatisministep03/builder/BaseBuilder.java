package com.mini.mybatis.mybatisministep03.builder;

import com.mini.mybatis.mybatisministep03.sqlsession.configuration.Configuration;

public class BaseBuilder {

    protected Configuration configuration;

    public BaseBuilder(Configuration configuration) {
        this.configuration = configuration;
    }

    public Configuration getConfiguration(){
        return configuration;
    }
}
