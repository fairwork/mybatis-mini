package com.mini.mybatis.mybatisministep03.sqlsession.defaults;

import com.mini.mybatis.mybatisministep03.mapping.MappedStatement;
import com.mini.mybatis.mybatisministep03.sqlsession.SqlSession;
import com.mini.mybatis.mybatisministep03.sqlsession.configuration.Configuration;

public class DefaultSqlSession implements SqlSession {

    private Configuration configuration;

    public DefaultSqlSession(Configuration configuration){
        this.configuration = configuration;
    }

    @Override
    public <T> T selectOne(String statement) {
        MappedStatement mappedStatement = configuration.getMapStatement(statement);
        return (T) ("被代理了 " + "statement" + mappedStatement.getId() + " sql语句 "+mappedStatement.getSql());
    }

    @Override
    public <T> T selectOne(String statement, Object parameter) {
        MappedStatement mappedStatement = configuration.getMapStatement(statement);
        return (T) ("被代理了 " + "statement" + mappedStatement.getId() + " sql语句 "+mappedStatement.getSql() + "parameter " + mappedStatement.getParameter());
    }

    @Override
    public <T> T getMapper(Class<T> type) {
        return configuration.getMapper(type,this);
    }

    @Override
    public Configuration getConfiguration() {
        return configuration;
    }
}
