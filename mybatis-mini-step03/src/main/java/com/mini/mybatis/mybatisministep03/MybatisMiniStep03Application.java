package com.mini.mybatis.mybatisministep03;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MybatisMiniStep03Application {

    public static void main(String[] args) {
        SpringApplication.run(MybatisMiniStep03Application.class, args);
    }

}
