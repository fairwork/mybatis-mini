package com.mini.mybatis.mybatisministep03.sqlsession;

public interface SqlSessionFactory {

    SqlSession openSession();

}
