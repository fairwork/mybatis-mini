package com.mini.mybatis.mybatisministep03.mapper;

public interface IPetMapper {

    String getPetNameById(Integer petId);

}
