package com.mini.mybatis.mybatisministep03.mapper;

public interface IUserMapper {

    String selectUserNameByUserId(Integer userId);

    String selectUserIdByUsername(String username);

}
