package com.mini.mybatis.mybatisministep03;

import com.mini.mybatis.mybatisministep03.builder.xml.XmlConfigBuilder;
import com.mini.mybatis.mybatisministep03.io.Resources;
import com.mini.mybatis.mybatisministep03.mapper.IUserMapper;
import com.mini.mybatis.mybatisministep03.sqlsession.SqlSession;
import com.mini.mybatis.mybatisministep03.sqlsession.SqlSessionFactory;
import com.mini.mybatis.mybatisministep03.sqlsession.SqlSessionFactoryBuilder;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;
import java.io.Reader;

@SpringBootTest
class MybatisMiniStep03ApplicationTests {

    @Test
    void contextLoads() {
    }


    @Test
    public void test() throws IOException {

        // 读取mybatis配置文件
        Reader reader = Resources.getResourceAsReader("mybatis-config.xml");
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(reader);

        SqlSession sqlSession = sqlSessionFactory.openSession();

        IUserMapper userMapper = sqlSession.getMapper(IUserMapper.class);
        String username = userMapper.selectUserNameByUserId(1111);
        System.out.println(username);

    }


}
