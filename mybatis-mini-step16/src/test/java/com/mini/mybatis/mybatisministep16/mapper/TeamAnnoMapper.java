package com.mini.mybatis.mybatisministep16.mapper;

import com.mini.mybatis.mybatisministep16.annotations.Select;
import com.mini.mybatis.mybatisministep16.entity.Team;

public interface TeamAnnoMapper {

    @Select("select * from team where team_id = #{teamId}")
    Team selectByAnno(Integer teamId);

}
