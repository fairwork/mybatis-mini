package com.mini.mybatis.mybatisministep16.datasource.pooled;

import com.mini.mybatis.mybatisministep16.datasource.unpooled.UnPooledDataSourceFactory;

public class PooledDataSourceFactory extends UnPooledDataSourceFactory {

    public PooledDataSourceFactory(){
        this.dataSource = new PooledDataSource();
    }

}
