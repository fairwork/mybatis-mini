package com.mini.mybatis.mybatisministep16.binding;

import com.mini.mybatis.mybatisministep16.sqlsession.SqlSession;

import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 *  1. MapperProxyFactory构造器，传入想要生成代理的mapper的类模板，完成初始化
 *  2. 通过newInstance方法，传入sqlSession对象，为目标mapper接口生成代理,并返回代理对象。
 *  tips：
 *      实现了invocationHandler方法的接口（mapperProxy），即是传入Proxy的第三个参数，其中的invoke()方法也是真正实现增强逻辑的地方
 * @param <T>
 */
public class MapperProxyFactory<T> {

    private final Class<T> mapperInterface;

    private Map<Method, MapperMethod> methodCache = new ConcurrentHashMap<>();


    public MapperProxyFactory(Class<T> mapperInterface) {
        this.mapperInterface = mapperInterface;
    }

    @SuppressWarnings("unchecked")
    public T newInstance(SqlSession sqlSession){
        final MapperProxy<T> mapperProxy = new MapperProxy<>(sqlSession,mapperInterface,methodCache);
        return (T) Proxy.newProxyInstance(mapperInterface.getClassLoader(),new Class[]{mapperInterface},mapperProxy);
    }
}
