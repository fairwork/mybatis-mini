package com.mini.mybatis.mybatisministep16.sqlsession;

public interface SqlSessionFactory {

    SqlSession openSession();

}
