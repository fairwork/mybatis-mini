package com.mini.mybatis.mybatisministep16.parsing;


public interface TokenHandler {

    String handleToken(String content);

}
