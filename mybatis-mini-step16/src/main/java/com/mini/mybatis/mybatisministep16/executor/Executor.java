package com.mini.mybatis.mybatisministep16.executor;

import com.mini.mybatis.mybatisministep16.mapping.BoundSql;
import com.mini.mybatis.mybatisministep16.mapping.MappedStatement;
import com.mini.mybatis.mybatisministep16.sqlsession.ResultHandler;
import com.mini.mybatis.mybatisministep16.sqlsession.RowBounds;
import com.mini.mybatis.mybatisministep16.transaction.Transaction;

import java.sql.SQLException;
import java.util.List;

/**
 * 顶层执行器接口
 */
public interface Executor {

    ResultHandler NO_RESULT_HANDLER = null;

    int update(MappedStatement ms, Object parameter) throws SQLException;

    <E> List<E> query(MappedStatement ms, Object parameter, RowBounds rowBounds, ResultHandler resultHandler, BoundSql boundSql) throws SQLException;

    <E> List<E> query(MappedStatement ms, Object parameter, RowBounds rowBounds, ResultHandler resultHandler) throws SQLException;


    Transaction getTransaction();

    void commit(boolean require) throws SQLException;

    void rollback(boolean require) throws SQLException;

    void close(boolean forceRollback);

}
