package com.mini.mybatis.mybatisministep16.plugin;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class InterceptorChain {

    private final List<Interceptor> interceptors = new ArrayList<>();

    // 拦截四大组件Executor、StatementHandler、parameterHandler、resultSetHandler
    public Object pluginAll(Object target) {
        for (Interceptor interceptor : interceptors) {
            target = interceptor.plugin(target);
        }
        return target;
    }

    public void addInterceptor(Interceptor interceptor) {
        interceptors.add(interceptor);
    }

    public List<Interceptor> getInterceptors(){
        return Collections.unmodifiableList(interceptors);
    }

}
