package com.mini.mybatis.mybatisministep16.sqlsession;

public interface ResultHandler {

    void handleResult(ResultContext context);

}
