package com.mini.mybatis.mybatisministep16.reflection.invoker;

public interface Invoker {

    Object invoke(Object object, Object[] args) throws Exception;

    Class<?> getType();

}
