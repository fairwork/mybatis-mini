package com.mini.mybatis.mybatisministep16.scripting.xmltags;


public interface SqlNode {

    boolean apply(DynamicContext context);

}