package com.mini.mybatis.mybatisministep16;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MybatisMiniStep16Application {

    public static void main(String[] args) {
        SpringApplication.run(MybatisMiniStep16Application.class, args);
    }

}
