package com.mini.mybatis.mybatisministep16.mapping;

public interface SqlSource {

    BoundSql getBoundSql(Object parameterObject);

}
