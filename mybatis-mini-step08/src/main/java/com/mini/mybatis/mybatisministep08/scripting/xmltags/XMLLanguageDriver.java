package com.mini.mybatis.mybatisministep08.scripting.xmltags;


import com.mini.mybatis.mybatisministep08.mapping.SqlSource;
import com.mini.mybatis.mybatisministep08.scripting.LanguageDriver;
import com.mini.mybatis.mybatisministep08.sqlsession.configuration.Configuration;
import org.dom4j.Element;


public class XMLLanguageDriver implements LanguageDriver {

    @Override
    public SqlSource createSqlSource(Configuration configuration, Element script, Class<?> parameterType) {
        // 用XML脚本构建器解析sql
        XMLScriptBuilder builder = new XMLScriptBuilder(configuration, script, parameterType);
        return builder.parseScriptNode();
    }

}