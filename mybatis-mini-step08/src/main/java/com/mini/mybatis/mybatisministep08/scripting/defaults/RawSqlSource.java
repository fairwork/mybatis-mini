package com.mini.mybatis.mybatisministep08.scripting.defaults;



import com.mini.mybatis.mybatisministep08.builder.SqlSourceBuilder;
import com.mini.mybatis.mybatisministep08.mapping.BoundSql;
import com.mini.mybatis.mybatisministep08.mapping.SqlSource;
import com.mini.mybatis.mybatisministep08.scripting.xmltags.DynamicContext;
import com.mini.mybatis.mybatisministep08.scripting.xmltags.SqlNode;
import com.mini.mybatis.mybatisministep08.sqlsession.configuration.Configuration;

import java.util.HashMap;


public class RawSqlSource implements SqlSource {

    // RawSqlSource 里组合的是 StaticSqlSource
    private final SqlSource sqlSource;

    public RawSqlSource(Configuration configuration, SqlNode rootSqlNode, Class<?> parameterType) {
        this(configuration, getSql(configuration, rootSqlNode), parameterType);
    }

    public RawSqlSource(Configuration configuration, String sql, Class<?> parameterType) {
        SqlSourceBuilder sqlSourceParser = new SqlSourceBuilder(configuration);
        Class<?> clazz = parameterType == null ? Object.class : parameterType;
        sqlSource = sqlSourceParser.parse(sql, clazz, new HashMap<>());
    }

    @Override
    public BoundSql getBoundSql(Object parameterObject) {
        return sqlSource.getBoundSql(parameterObject);
    }

    private static String getSql(Configuration configuration, SqlNode rootSqlNode) {
        DynamicContext context = new DynamicContext(configuration, null);
        rootSqlNode.apply(context);
        return context.getSql();
    }

}
