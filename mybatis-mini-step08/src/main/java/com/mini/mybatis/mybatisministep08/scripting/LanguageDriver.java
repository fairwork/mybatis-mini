package com.mini.mybatis.mybatisministep08.scripting;


import com.mini.mybatis.mybatisministep08.mapping.SqlSource;
import com.mini.mybatis.mybatisministep08.sqlsession.configuration.Configuration;
import org.dom4j.Element;


public interface LanguageDriver {

    SqlSource createSqlSource(Configuration configuration, Element script, Class<?> parameterType);

}
