package com.mini.mybatis.mybatisministep08;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MybatisMiniStep08Application {

    public static void main(String[] args) {
        SpringApplication.run(MybatisMiniStep08Application.class, args);
    }

}
