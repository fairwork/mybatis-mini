package com.mini.mybatis.mybatisministep08.datasource.pooled;

import com.mini.mybatis.mybatisministep08.datasource.unpooled.UnPooledDataSourceFactory;

public class PooledDataSourceFactory extends UnPooledDataSourceFactory {

    public PooledDataSourceFactory(){
        this.dataSource = new PooledDataSource();
    }

}
