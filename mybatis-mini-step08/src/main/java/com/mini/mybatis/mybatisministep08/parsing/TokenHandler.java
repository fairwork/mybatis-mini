package com.mini.mybatis.mybatisministep08.parsing;


public interface TokenHandler {

    String handleToken(String content);

}
