package com.mini.mybatis.mybatisministep08.mapping;

public interface SqlSource {

    BoundSql getBoundSql(Object parameterObject);

}
