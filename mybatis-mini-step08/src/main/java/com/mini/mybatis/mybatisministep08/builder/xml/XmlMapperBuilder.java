package com.mini.mybatis.mybatisministep08.builder.xml;

import com.mini.mybatis.mybatisministep08.builder.BaseBuilder;
import com.mini.mybatis.mybatisministep08.io.Resources;
import com.mini.mybatis.mybatisministep08.sqlsession.configuration.Configuration;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import java.io.InputStream;
import java.util.List;

/**
 * 映射构造器
 */
public class XmlMapperBuilder extends BaseBuilder {

    private Element element;

    private String resource;

    private String currentNameSpace;


    public XmlMapperBuilder(InputStream inputStream, Configuration configuration, String resource) throws DocumentException {
        this(new SAXReader().read(inputStream),configuration,resource);
    }

    public XmlMapperBuilder(Document document, Configuration configuration, String resource) {
        super(configuration);
        this.element = document.getRootElement();
        this.resource = resource;
    }


    public void parse() throws ClassNotFoundException {
        // 如果已经解析过则不需要再次解析
        if (!configuration.isResourceLoaded(resource)){
            // 解析当前xml，从根节点开始解析
            configurationElement(element);
            // 解析完成后，标记一下
            configuration.addLoadedResource(resource);
            // 绑定映射器注册器-namespace
            configuration.addMapper(Resources.classForName(currentNameSpace));
        }

    }

    private void configurationElement(Element element) {
        //1. 配置currentNameSpace
        currentNameSpace = element.attributeValue("namespace");
        if (currentNameSpace.equals("")){
            throw new RuntimeException("Mapper's namespace cannot be empty");
        }

        //2. select|update|insert|delete
        buildStatementFromContext(element.elements("select"));
    }

    private void buildStatementFromContext(List<Element> elementList) {
        // 解析每一个sql标签
        for (Element element : elementList) {
            // 每一个标签都要创建一个XmlStatementBuilder去解析
            XmlStatementBuilder xmlStatementBuilder = new XmlStatementBuilder(configuration, element, currentNameSpace);
            xmlStatementBuilder.parse();
        }


    }
}
