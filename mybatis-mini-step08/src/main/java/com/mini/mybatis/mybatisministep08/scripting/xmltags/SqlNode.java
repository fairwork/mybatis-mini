package com.mini.mybatis.mybatisministep08.scripting.xmltags;


public interface SqlNode {

    boolean apply(DynamicContext context);

}