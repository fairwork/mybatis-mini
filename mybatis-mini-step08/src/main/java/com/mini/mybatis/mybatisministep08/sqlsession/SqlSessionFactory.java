package com.mini.mybatis.mybatisministep08.sqlsession;

public interface SqlSessionFactory {

    SqlSession openSession();

}
