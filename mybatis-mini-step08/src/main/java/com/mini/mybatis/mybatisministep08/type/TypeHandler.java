package com.mini.mybatis.mybatisministep08.type;

import java.sql.PreparedStatement;

public interface TypeHandler<T> {

    void setParameter(PreparedStatement ps, int i, T parameter, JdbcType jdbcType) throws Exception;

}
