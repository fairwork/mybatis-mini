package com.mini.mybatis.mybatisministep08.type;

import java.lang.reflect.Type;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;

/**
 * 类型处理器注册器
 */
public class TypeHandlerRegistry {

    private final Map<JdbcType, TypeHandler<?>> JDBC_TYPE_HANDLER_MAP = new EnumMap<>(JdbcType.class);
    private final Map<Type, Map<JdbcType, TypeHandler<?>>> TYPE_HANDLER_MAP = new HashMap<>();
    private final Map<Class<?>, TypeHandler<?>> ALL_TYPE_HANDLERS_MAP = new HashMap<>();

    public TypeHandlerRegistry() {

    }

    public void registry(Type javaType, JdbcType jdbcType, TypeHandler<?> typeHandler){
        if (javaType != null){
            Map<JdbcType, TypeHandler<?>> map = TYPE_HANDLER_MAP.computeIfAbsent(javaType, key -> new HashMap<>());
            map.put(jdbcType,typeHandler);
        }
        ALL_TYPE_HANDLERS_MAP.put(typeHandler.getClass(),typeHandler);
    }

}
