package com.mini.mybatis.mybatisministep08.mapper;

import com.mini.mybatis.mybatisministep08.entity.Team;

public interface TeamMapper {


    Team selectByTeamId(Integer teamId);

}
