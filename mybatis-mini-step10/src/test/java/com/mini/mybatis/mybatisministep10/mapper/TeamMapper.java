package com.mini.mybatis.mybatisministep10.mapper;

import com.mini.mybatis.mybatisministep10.entity.Team;

public interface TeamMapper {


    Team selectByTeamId(Integer teamId);

    Team selectByTeamName(String teamName);

}
