package com.mini.mybatis.mybatisministep10.datasource.pooled;

import com.mini.mybatis.mybatisministep10.datasource.unpooled.UnPooledDataSourceFactory;

public class PooledDataSourceFactory extends UnPooledDataSourceFactory {

    public PooledDataSourceFactory(){
        this.dataSource = new PooledDataSource();
    }

}
