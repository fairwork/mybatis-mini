package com.mini.mybatis.mybatisministep10.mapping;


import com.mini.mybatis.mybatisministep10.sqlsession.configuration.Configuration;
import com.mini.mybatis.mybatisministep10.type.JdbcType;
import com.mini.mybatis.mybatisministep10.type.TypeHandler;


public class ResultMapping {

    private Configuration configuration;
    private String property;
    private String column;
    private Class<?> javaType;
    private JdbcType jdbcType;
    private TypeHandler<?> typeHandler;

    ResultMapping() {
    }

    public static class Builder {
        private ResultMapping resultMapping = new ResultMapping();


    }

}
