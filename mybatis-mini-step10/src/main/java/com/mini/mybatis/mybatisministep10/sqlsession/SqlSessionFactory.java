package com.mini.mybatis.mybatisministep10.sqlsession;

public interface SqlSessionFactory {

    SqlSession openSession();

}
