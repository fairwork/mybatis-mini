package com.mini.mybatis.mybatisministep10.scripting.xmltags;


public interface SqlNode {

    boolean apply(DynamicContext context);

}