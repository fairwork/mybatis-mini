package com.mini.mybatis.mybatisministep10.sqlsession;


public interface ResultContext {

    /**
     * 获取结果
     */
    Object getResultObject();

    /**
     * 获取记录数
     */
    int getResultCount();

}
