package com.mini.mybatis.mybatisministep10.parsing;


public interface TokenHandler {

    String handleToken(String content);

}
