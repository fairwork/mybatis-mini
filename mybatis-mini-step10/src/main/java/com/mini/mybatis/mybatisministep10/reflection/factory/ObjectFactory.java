package com.mini.mybatis.mybatisministep10.reflection.factory;

import java.util.List;
import java.util.Properties;

/**
 *  默认的对象工厂，所有的对象都由工厂来生成
 *  我们统一通过对象工厂来创建对象，通过得知 Class<?> clazz，而不是各自通过clazz去获取构造器，进而newInstance()
 *  工厂模式
 */
public interface ObjectFactory {

    /**
     * Sets configuration properties.
     * 设置属性
     * @param properties configuration properties
     */
    void setProperties(Properties properties);

    /**
     * Creates a new object with default constructor.
     * 生产对象
     * @param type Object type
     * @return <T>
     */
    <T> T create(Class<T> type);

    /**
     * Creates a new object with the specified constructor and params.
     * 生产对象，使用指定的构造函数和构造函数参数
     * @param type Object type
     * @param constructorArgTypes Constructor argument types
     * @param constructorArgs Constructor argument values
     * @return <T>
     */
    <T> T create(Class<T> type, List<Class<?>> constructorArgTypes, List<Object> constructorArgs);

    /**
     * Returns true if this object can have a set of other objects.
     * It's main purpose is to support non-java.util.Collection objects like Scala collections.
     * 返回这个对象是否是集合，为了支持 Scala collections
     *
     * @since 3.1.0
     * @param type Object type
     * @return whether it is a collection or not
     */
    <T> boolean isCollection(Class<T> type);

}
