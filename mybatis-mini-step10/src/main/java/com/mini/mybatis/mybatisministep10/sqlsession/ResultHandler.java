package com.mini.mybatis.mybatisministep10.sqlsession;

public interface ResultHandler {

    void handleResult(ResultContext context);

}
