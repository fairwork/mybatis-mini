package com.mini.mybatis.mybatisministep10.reflection.invoker;

import java.lang.reflect.Method;

public class MethodInvoker implements Invoker{

    private Class<?> type;

    private Method method;

    public MethodInvoker(Method method){
        this.method = method;

        // 如果只有一个参数，返回参数类型，否则返回return类型
        if (method.getParameterTypes().length == 1){
            this.type = method.getParameterTypes()[0];
        }else {
            this.type = method.getReturnType();
        }
    }

    @Override
    public Object invoke(Object object, Object[] args) throws Exception {
        return method.invoke(object,args);
    }

    @Override
    public Class<?> getType() {
        return type;
    }


}
