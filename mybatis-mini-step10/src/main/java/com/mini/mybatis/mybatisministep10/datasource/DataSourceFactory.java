package com.mini.mybatis.mybatisministep10.datasource;

import javax.sql.DataSource;
import java.util.Properties;

public interface DataSourceFactory {

    /**
     * 通过反射的方法设置属性，这样就不用采取硬编码的方式
     * @param properties
     */
    void setProperties(Properties properties);

    DataSource getDataSource();

}
