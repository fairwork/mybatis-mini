package com.mini.mybatis.mybatisministep10.mapping;

public interface SqlSource {

    BoundSql getBoundSql(Object parameterObject);

}
