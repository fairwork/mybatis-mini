package com.mini.mybatis.mybatisministep10;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MybatisMiniStep10Application {

    public static void main(String[] args) {
        SpringApplication.run(MybatisMiniStep10Application.class, args);
    }

}
