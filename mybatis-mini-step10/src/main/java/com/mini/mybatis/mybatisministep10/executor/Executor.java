package com.mini.mybatis.mybatisministep10.executor;

import com.mini.mybatis.mybatisministep10.mapping.BoundSql;
import com.mini.mybatis.mybatisministep10.mapping.MappedStatement;
import com.mini.mybatis.mybatisministep10.sqlsession.ResultHandler;
import com.mini.mybatis.mybatisministep10.sqlsession.RowBounds;
import com.mini.mybatis.mybatisministep10.transaction.Transaction;

import java.sql.SQLException;
import java.util.List;

/**
 * 顶层执行器接口
 */
public interface Executor {

    ResultHandler NO_RESULT_HANDLER = null;

    <E> List<E> query(MappedStatement ms, Object parameter, RowBounds rowBounds, ResultHandler resultHandler, BoundSql boundSql);

    Transaction getTransaction();

    void commit(boolean require) throws SQLException;

    void rollback(boolean require) throws SQLException;

    void close(boolean forceRollback);

}
