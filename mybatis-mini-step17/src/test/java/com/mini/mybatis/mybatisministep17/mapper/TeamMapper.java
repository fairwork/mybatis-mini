package com.mini.mybatis.mybatisministep17.mapper;

import com.mini.mybatis.mybatisministep17.entity.Team;

import java.util.List;

public interface TeamMapper {


    Team selectByTeamId(Integer teamId);

    List<Team> selectByTeamIdDyn(Team team);

    Team selectByTeamName(String teamName);

    List<Team> selectAllTeam();

    void insert(Team team);

    int update(Team team);

    int delete(Integer teamId);



}
