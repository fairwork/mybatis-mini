package com.mini.mybatis.mybatisministep17.mapper;

import com.mini.mybatis.mybatisministep17.annotations.Select;
import com.mini.mybatis.mybatisministep17.entity.Team;

public interface TeamAnnoMapper {

    @Select("select * from team where team_id = #{teamId}")
    Team selectByAnno(Integer teamId);

}
