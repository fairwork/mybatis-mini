package com.mini.mybatis.mybatisministep17.sqlsession;

public interface ResultHandler {

    void handleResult(ResultContext context);

}
