package com.mini.mybatis.mybatisministep17.parsing;


public interface TokenHandler {

    String handleToken(String content);

}
