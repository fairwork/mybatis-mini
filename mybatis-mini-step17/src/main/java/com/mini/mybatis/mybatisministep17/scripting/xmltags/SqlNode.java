package com.mini.mybatis.mybatisministep17.scripting.xmltags;


public interface SqlNode {

    boolean apply(DynamicContext context);

}