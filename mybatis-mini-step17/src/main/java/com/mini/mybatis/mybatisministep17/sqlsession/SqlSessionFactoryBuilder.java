package com.mini.mybatis.mybatisministep17.sqlsession;

import com.mini.mybatis.mybatisministep17.builder.xml.XmlConfigBuilder;
import com.mini.mybatis.mybatisministep17.sqlsession.configuration.Configuration;
import com.mini.mybatis.mybatisministep17.sqlsession.defaults.DefaultSqlSessionFactory;

import java.io.Reader;

/**
 * MyBatis的入口
 */
public class SqlSessionFactoryBuilder {


    public SqlSessionFactory build(Reader reader){
        XmlConfigBuilder xmlConfigBuilder = new XmlConfigBuilder(reader);
        return build(xmlConfigBuilder.parse());
    }

    public SqlSessionFactory build(Configuration configuration){
        return new DefaultSqlSessionFactory(configuration);
    }


}
