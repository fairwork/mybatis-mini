package com.mini.mybatis.mybatisministep17.mapping;

public interface SqlSource {

    BoundSql getBoundSql(Object parameterObject);

}
