package com.mini.mybatis.mybatisministep17.reflection.invoker;

import java.lang.reflect.Field;

public class GetFieldInvoker implements Invoker {


    private Field field;


    public GetFieldInvoker(Field field) {
        this.field = field;
    }

    @Override
    public Object invoke(Object object, Object[] args) throws Exception {
        return field.get(object);
    }

    @Override
    public Class<?> getType() {
        return field.getType();
    }
}
