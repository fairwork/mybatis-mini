package com.mini.mybatis.mybatisministep17.sqlsession;

public interface SqlSessionFactory {

    SqlSession openSession();

}
