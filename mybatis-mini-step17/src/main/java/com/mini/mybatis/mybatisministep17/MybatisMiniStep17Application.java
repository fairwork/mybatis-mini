package com.mini.mybatis.mybatisministep17;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MybatisMiniStep17Application {

    public static void main(String[] args) {
        SpringApplication.run(MybatisMiniStep17Application.class, args);
    }

}
