package com.mini.mybatis.mybatisministep04;

import com.mini.mybatis.mybatisministep04.entity.Team;
import com.mini.mybatis.mybatisministep04.io.Resources;
import com.mini.mybatis.mybatisministep04.mapper.TeamMapper;
import com.mini.mybatis.mybatisministep04.sqlsession.SqlSession;
import com.mini.mybatis.mybatisministep04.sqlsession.SqlSessionFactory;
import com.mini.mybatis.mybatisministep04.sqlsession.SqlSessionFactoryBuilder;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;

@SpringBootTest
class MybatisMiniStep04ApplicationTests {

    @Test
    void contextLoads() {
    }


    @Test
    public void test() throws IOException {
        // 根据配置文件获取 sqlSessionFactory
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(Resources.getResourceAsReader("mybatis-config.xml"));
        // 这次的openSession，底层不再是模拟调用数据库执行sql，而是从配置的数据源中获取数据库链接，执行sql，返回结果
        SqlSession sqlSession = sqlSessionFactory.openSession();
        TeamMapper teamMapper = sqlSession.getMapper(TeamMapper.class);
        Team team = teamMapper.selectByTeamId(1005);
        System.out.println(team);

    }

}
