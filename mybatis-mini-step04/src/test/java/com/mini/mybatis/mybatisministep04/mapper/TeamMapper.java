package com.mini.mybatis.mybatisministep04.mapper;

import com.mini.mybatis.mybatisministep04.entity.Team;

public interface TeamMapper {


    Team selectByTeamId(Integer teamId);

}
