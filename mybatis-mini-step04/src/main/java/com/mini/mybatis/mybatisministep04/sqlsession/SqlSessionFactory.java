package com.mini.mybatis.mybatisministep04.sqlsession;

public interface SqlSessionFactory {

    SqlSession openSession();

}
