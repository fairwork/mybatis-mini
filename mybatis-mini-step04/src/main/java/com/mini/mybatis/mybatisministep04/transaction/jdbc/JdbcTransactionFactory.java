package com.mini.mybatis.mybatisministep04.transaction.jdbc;

import com.mini.mybatis.mybatisministep04.sqlsession.TransactionIsolationLevel;
import com.mini.mybatis.mybatisministep04.transaction.Transaction;
import com.mini.mybatis.mybatisministep04.transaction.TransactionFactory;

import javax.sql.DataSource;
import java.sql.Connection;

/**
 * xml版本的mybatis，默认使用JdbcTransactionFactory，
 * 若要与spring整合，就必须替换为SpringManagedTransactionFactory,将事务交给spring管理
 */
public class JdbcTransactionFactory implements TransactionFactory {

    @Override
    public Transaction newTransaction(Connection conn) {
        return new JdbcTransaction(conn);
    }

    @Override
    public Transaction newTransaction(DataSource dataSource, TransactionIsolationLevel level, boolean autoCommit) {
        return new JdbcTransaction(dataSource, level, autoCommit);
    }
}
