package com.mini.mybatis.mybatisministep04.sqlsession.defaults;

import com.mini.mybatis.mybatisministep04.sqlsession.SqlSession;
import com.mini.mybatis.mybatisministep04.sqlsession.SqlSessionFactory;
import com.mini.mybatis.mybatisministep04.sqlsession.configuration.Configuration;

public class DefaultSqlSessionFactory implements SqlSessionFactory {

    private Configuration configuration;

    public DefaultSqlSessionFactory(Configuration configuration) {
        this.configuration = configuration;
    }

    @Override
    public SqlSession openSession() {
        return new DefaultSqlSession(configuration);
    }
}
