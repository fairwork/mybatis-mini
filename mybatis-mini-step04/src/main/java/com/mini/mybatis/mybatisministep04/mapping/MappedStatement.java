package com.mini.mybatis.mybatisministep04.mapping;

import com.mini.mybatis.mybatisministep04.sqlsession.configuration.Configuration;

import java.util.Map;

public class MappedStatement {

    // 全局配置
    private Configuration configuration;

    // 每个MappedStatement的唯一id
    private String id;

    // sql类型
    private SqlCommandType sqlCommandType;

    private BoundSql boundSql;


    MappedStatement(){

    }


    /**
     * 建造者模式，不许外部直接new MappedStatement对象
     */
    public static class Builder{

        private MappedStatement mappedStatement = new MappedStatement();


        public Builder(Configuration configuration, String id, SqlCommandType sqlCommandType, BoundSql boundSql) {
            mappedStatement.configuration = configuration;
            mappedStatement.id = id;
            mappedStatement.sqlCommandType = sqlCommandType;
            mappedStatement.boundSql = boundSql;
        }

        public MappedStatement build(){
            assert mappedStatement.configuration != null;
            assert mappedStatement.id != null;
            return mappedStatement;
        }

    }


    public Configuration getConfiguration() {
        return configuration;
    }

    public void setConfiguration(Configuration configuration) {
        this.configuration = configuration;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public SqlCommandType getSqlCommandType() {
        return sqlCommandType;
    }

    public void setSqlCommandType(SqlCommandType sqlCommandType) {
        this.sqlCommandType = sqlCommandType;
    }

    public BoundSql getBoundSql() {
        return boundSql;
    }

    public void setBoundSql(BoundSql boundSql) {
        this.boundSql = boundSql;
    }
}
