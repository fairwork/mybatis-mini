package com.mini.mybatis.mybatisministep04.builder;

import com.mini.mybatis.mybatisministep04.sqlsession.configuration.Configuration;
import com.mini.mybatis.mybatisministep04.type.TypeAliasRegistry;

public class BaseBuilder {

    protected Configuration configuration;

    protected TypeAliasRegistry typeAliasRegistry;

    public BaseBuilder(Configuration configuration) {
        this.configuration = configuration;
        typeAliasRegistry = this.configuration.getTypeAliasRegistry();
    }

    public Configuration getConfiguration(){
        return configuration;
    }
}
