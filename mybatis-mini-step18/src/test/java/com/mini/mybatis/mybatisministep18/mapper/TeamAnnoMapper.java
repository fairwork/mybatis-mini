package com.mini.mybatis.mybatisministep18.mapper;

import com.mini.mybatis.mybatisministep18.annotations.Select;
import com.mini.mybatis.mybatisministep18.entity.Team;

public interface TeamAnnoMapper {

    @Select("select * from team where team_id = #{teamId}")
    Team selectByAnno(Integer teamId);

}
