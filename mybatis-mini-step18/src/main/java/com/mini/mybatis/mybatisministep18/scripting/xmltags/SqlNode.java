package com.mini.mybatis.mybatisministep18.scripting.xmltags;


public interface SqlNode {

    boolean apply(DynamicContext context);

}