package com.mini.mybatis.mybatisministep18.sqlsession;

public interface SqlSessionFactory {

    SqlSession openSession();

}
