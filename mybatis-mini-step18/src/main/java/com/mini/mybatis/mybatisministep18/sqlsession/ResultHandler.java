package com.mini.mybatis.mybatisministep18.sqlsession;

public interface ResultHandler {

    void handleResult(ResultContext context);

}
