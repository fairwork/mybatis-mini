package com.mini.mybatis.mybatisministep18.parsing;


public interface TokenHandler {

    String handleToken(String content);

}
