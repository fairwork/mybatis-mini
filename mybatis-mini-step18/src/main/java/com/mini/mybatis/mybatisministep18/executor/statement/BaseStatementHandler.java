package com.mini.mybatis.mybatisministep18.executor.statement;

import com.mini.mybatis.mybatisministep18.executor.Executor;
import com.mini.mybatis.mybatisministep18.executor.keygen.KeyGenerator;
import com.mini.mybatis.mybatisministep18.executor.parameter.ParameterHandler;
import com.mini.mybatis.mybatisministep18.executor.resultset.ResultSetHandler;
import com.mini.mybatis.mybatisministep18.mapping.BoundSql;
import com.mini.mybatis.mybatisministep18.mapping.MappedStatement;
import com.mini.mybatis.mybatisministep18.sqlsession.ResultHandler;
import com.mini.mybatis.mybatisministep18.sqlsession.RowBounds;
import com.mini.mybatis.mybatisministep18.sqlsession.configuration.Configuration;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public abstract class BaseStatementHandler implements StatementHandler{

    protected Configuration configuration;

    protected Executor executor;

    protected MappedStatement mappedStatement;

    protected Object parameterObject;

    protected ResultSetHandler resultSetHandler;

    protected final ParameterHandler parameterHandler;

    protected final RowBounds rowBounds;
    protected BoundSql boundSql;


    public BaseStatementHandler(Executor executor, MappedStatement mappedStatement, Object parameterObject, RowBounds rowBounds, ResultHandler resultHandler, BoundSql boundSql) {
        this.configuration = mappedStatement.getConfiguration();
        this.executor = executor;
        this.mappedStatement = mappedStatement;
        this.rowBounds = rowBounds;

        // 因为update 不会传入boundSql参数，所以需要做初始化处理
        if (boundSql == null){
            generateKeys(parameterObject);
            boundSql = mappedStatement.getBoundSql(parameterObject);
        }

        this.boundSql = boundSql;
        this.parameterObject = parameterObject;
        this.parameterHandler = configuration.newParameterHandler(mappedStatement,parameterObject,boundSql);
        this.resultSetHandler = configuration.newResultSetHandler(executor,mappedStatement,rowBounds,resultHandler,boundSql);
    }


    @Override
    public Statement prepare(Connection connection) throws SQLException {
        Statement statement = null;
        try {
            // 实例化 Statement
            statement = instantiateStatement(connection);
            // 参数设置，可以被抽取，提供配置
            statement.setQueryTimeout(350);
            statement.setFetchSize(10000);
            return statement;
        } catch (Exception e) {
            throw new RuntimeException("Error preparing statement.  Cause: " + e, e);
        }
    }

    @Override
    public BoundSql getBoundSql() {
        return boundSql;
    }

    protected abstract Statement instantiateStatement(Connection connection) throws SQLException;

    protected void generateKeys(Object parameter) {
        KeyGenerator keyGenerator = mappedStatement.getKeyGenerator();
        keyGenerator.processBefore(executor, mappedStatement, null, parameter);
    }
}
