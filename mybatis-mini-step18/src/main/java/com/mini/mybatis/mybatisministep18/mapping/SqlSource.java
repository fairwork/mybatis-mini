package com.mini.mybatis.mybatisministep18.mapping;

public interface SqlSource {

    BoundSql getBoundSql(Object parameterObject);

}
