package com.mini.mybatis.mybatisministep18.transaction;

import com.mini.mybatis.mybatisministep18.sqlsession.TransactionIsolationLevel;

import javax.sql.DataSource;
import java.sql.Connection;

public interface TransactionFactory {

    /**
     * 根据connection创建Transaction
     * @param connection
     * @return
     */
    Transaction newTransaction(Connection connection);



    /**
     * 根据datasource、autoCommit、TransactionLevel创建Transaction
     * @param dataSource
     * @param level
     * @param autoCommit
     * @return
     */
    Transaction newTransaction(DataSource dataSource, TransactionIsolationLevel level, boolean autoCommit);


}
