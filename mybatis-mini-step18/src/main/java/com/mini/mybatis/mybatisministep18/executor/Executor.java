package com.mini.mybatis.mybatisministep18.executor;

import com.mini.mybatis.mybatisministep18.cache.CacheKey;
import com.mini.mybatis.mybatisministep18.mapping.BoundSql;
import com.mini.mybatis.mybatisministep18.mapping.MappedStatement;
import com.mini.mybatis.mybatisministep18.sqlsession.ResultHandler;
import com.mini.mybatis.mybatisministep18.sqlsession.RowBounds;
import com.mini.mybatis.mybatisministep18.transaction.Transaction;

import java.sql.SQLException;
import java.util.List;

/**
 * 顶层执行器接口
 */
public interface Executor {

    ResultHandler NO_RESULT_HANDLER = null;

    int update(MappedStatement ms, Object parameter) throws SQLException;

    <E> List<E> query(MappedStatement ms, Object parameter, RowBounds rowBounds, ResultHandler resultHandler, CacheKey key, BoundSql boundSql) throws SQLException;

    <E> List<E> query(MappedStatement ms, Object parameter, RowBounds rowBounds, ResultHandler resultHandler) throws SQLException;


    Transaction getTransaction();

    void commit(boolean require) throws SQLException;

    void rollback(boolean require) throws SQLException;

    void close(boolean forceRollback);

    // 清理Session缓存
    void clearLocalCache();

    // 创建缓存 Key
    CacheKey createCacheKey(MappedStatement ms, Object parameterObject, RowBounds rowBounds, BoundSql boundSql);

    void setExecutorWrapper(Executor executor);
}
