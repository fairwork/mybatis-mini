package com.mini.mybatis.mybatisministep18;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MybatisMiniStep18Application {

    public static void main(String[] args) {
        SpringApplication.run(MybatisMiniStep18Application.class, args);
    }

}
