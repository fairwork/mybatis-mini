package com.mini.mybatis.mybatisministep18.sqlsession;

import com.mini.mybatis.mybatisministep18.sqlsession.configuration.Configuration;

import java.util.List;

public interface SqlSession {


    /**
     * 根据指定的sqlId获取封装对象
     * @param statement
     * @return
     * @param <T>
     */
    <T> T selectOne(String statement);



    /**
     * 根据指定的sqlId和参数获取封装对象
     * @param statement
     * @param parameter
     * @return
     * @param <T>
     */
    <T> T selectOne(String statement,Object parameter);


    /**
     * Retrieve a list of mapped objects from the statement key and parameter.
     * 获取多条记录，这个方法容许我们可以传递一些参数
     *
     * @param <E>       the returned list element type
     * @param statement Unique identifier matching the statement to use.
     * @param parameter A parameter object to pass to the statement.
     * @return List of mapped object
     */
    <E> List<E> selectList(String statement, Object parameter);

    /**
     * Execute an insert statement with the given parameter object. Any generated
     * autoincrement values or selectKey entries will modify the given parameter
     * object properties. Only the number of rows affected will be returned.
     * 插入记录，容许传入参数。
     *
     * @param statement Unique identifier matching the statement to execute.
     * @param parameter A parameter object to pass to the statement.
     * @return int The number of rows affected by the insert. 注意返回的是受影响的行数
     */
    int insert(String statement, Object parameter);

    /**
     * Execute an update statement. The number of rows affected will be returned.
     * 更新记录
     *
     * @param statement Unique identifier matching the statement to execute.
     * @param parameter A parameter object to pass to the statement.
     * @return int The number of rows affected by the update. 返回的是受影响的行数
     */
    int update(String statement, Object parameter);

    /**
     * Execute a delete statement. The number of rows affected will be returned.
     * 删除记录
     *
     * @param statement Unique identifier matching the statement to execute.
     * @param parameter A parameter object to pass to the statement.
     * @return int The number of rows affected by the delete. 返回的是受影响的行数
     */
    Object delete(String statement, Object parameter);


    /**
     * 以下是事务控制方法 commit,rollback
     * Flushes batch statements and commits database connection.
     * Note that database connection will not be committed if no updates/deletes/inserts were called.
     */
    void commit();

    /**
     * 关闭Session
     */
    void close();

    /**
     * 清理 Session 缓存
     */
    void clearCache();

    /**
     * 根据mapper类Class获取对应类型MapperProxyFactory
     * 巧妙运用泛型
     * @param type
     * @return
     * @param <T>
     */
    <T> T getMapper(Class<T> type);


    Configuration getConfiguration();

}
