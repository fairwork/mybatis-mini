package com.mini.mybatis.mybatisministep15.scripting.xmltags;


public interface SqlNode {

    boolean apply(DynamicContext context);

}