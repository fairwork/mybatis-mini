package com.mini.mybatis.mybatisministep15.sqlsession;

public interface SqlSessionFactory {

    SqlSession openSession();

}
