package com.mini.mybatis.mybatisministep15.executor;

import com.mini.mybatis.mybatisministep15.executor.statement.StatementHandler;
import com.mini.mybatis.mybatisministep15.mapping.BoundSql;
import com.mini.mybatis.mybatisministep15.mapping.MappedStatement;
import com.mini.mybatis.mybatisministep15.sqlsession.ResultHandler;
import com.mini.mybatis.mybatisministep15.sqlsession.RowBounds;
import com.mini.mybatis.mybatisministep15.sqlsession.configuration.Configuration;
import com.mini.mybatis.mybatisministep15.transaction.Transaction;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

public class SimpleExecutor extends BaseExecutor{

    public SimpleExecutor(Configuration configuration, Transaction transaction) {
        super(configuration, transaction);
    }

    @Override
    protected int doUpdate(MappedStatement ms, Object parameter) throws SQLException {
        Statement stmt = null;
        try {
            Configuration configuration = ms.getConfiguration();
            // 新建一个 StatementHandler
            StatementHandler handler = configuration.newStatementHandler(this, ms, parameter, RowBounds.DEFAULT, null, null);
            // 准备语句
            stmt = prepareStatement(handler);
            // StatementHandler.update
            return handler.update(stmt);
        }catch (Exception e){
            e.printStackTrace();
            throw new RuntimeException(e.getCause());
        }finally {
            closeStatement(stmt);
        }
    }

    @Override
    protected <E> List<E> doQuery(MappedStatement ms, Object parameter, RowBounds rowBounds, ResultHandler resultHandler, BoundSql boundSql) throws SQLException {
        Statement stmt = null;
        try {
            Configuration configuration = ms.getConfiguration();
            // 新建一个StatementHandler
            StatementHandler handler = configuration.newStatementHandler(this,ms,parameter,rowBounds,resultHandler,boundSql);
            // 准备语句
            stmt = prepareStatement(handler);
            // 返回结果
            return handler.query(stmt,resultHandler);
        }catch (Exception e){
            e.printStackTrace();
            throw new RuntimeException(e.getCause());
        }finally {
            closeStatement(stmt);
        }
    }

    private Statement prepareStatement(StatementHandler handler) throws SQLException {
        Statement stmt;
        Connection connection = transaction.getConnection();
        // 准备语句
        stmt = handler.prepare(connection);
        handler.parameterize(stmt);
        return stmt;
    }
}
