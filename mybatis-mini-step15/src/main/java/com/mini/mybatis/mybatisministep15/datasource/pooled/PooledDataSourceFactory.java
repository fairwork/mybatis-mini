package com.mini.mybatis.mybatisministep15.datasource.pooled;

import com.mini.mybatis.mybatisministep15.datasource.unpooled.UnPooledDataSourceFactory;

public class PooledDataSourceFactory extends UnPooledDataSourceFactory {

    public PooledDataSourceFactory(){
        this.dataSource = new PooledDataSource();
    }

}
