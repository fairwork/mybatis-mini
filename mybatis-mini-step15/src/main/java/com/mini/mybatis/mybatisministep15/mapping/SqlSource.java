package com.mini.mybatis.mybatisministep15.mapping;

public interface SqlSource {

    BoundSql getBoundSql(Object parameterObject);

}
