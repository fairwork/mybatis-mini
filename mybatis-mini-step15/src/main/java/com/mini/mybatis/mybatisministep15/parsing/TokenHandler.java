package com.mini.mybatis.mybatisministep15.parsing;


public interface TokenHandler {

    String handleToken(String content);

}
