package com.mini.mybatis.mybatisministep15;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MybatisMiniStep15Application {

    public static void main(String[] args) {
        SpringApplication.run(MybatisMiniStep15Application.class, args);
    }

}
