package com.mini.mybatis.mybatisministep15.sqlsession;

public interface ResultHandler {

    void handleResult(ResultContext context);

}
