package com.mini.mybatis.mybatisministep15.reflection.invoker;

import java.lang.reflect.Field;

public class SetFieldInvoker implements Invoker{

    private Field field;

    public SetFieldInvoker(Field field) {
        this.field = field;
    }

    @Override
    public Object invoke(Object object, Object[] args) throws Exception {
        field.set(object,args[0]);
        return null;
    }

    @Override
    public Class<?> getType() {
        return field.getType();
    }
}
