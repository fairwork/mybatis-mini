package com.mini.mybatis.mybatisministep15.mapper;

import com.mini.mybatis.mybatisministep15.annotations.Select;
import com.mini.mybatis.mybatisministep15.entity.Team;

public interface TeamAnnoMapper {

    @Select("select * from team where team_id = #{teamId}")
    Team selectByAnno(Integer teamId);

}
