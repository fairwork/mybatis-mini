package com.mini.mybatis.mybatisministep15;

import com.mini.mybatis.mybatisministep15.entity.Team;
import com.mini.mybatis.mybatisministep15.io.Resources;
import com.mini.mybatis.mybatisministep15.mapper.TeamAnnoMapper;
import com.mini.mybatis.mybatisministep15.mapper.TeamMapper;
import com.mini.mybatis.mybatisministep15.reflection.MetaObject;
import com.mini.mybatis.mybatisministep15.reflection.SystemMetaObject;
import com.mini.mybatis.mybatisministep15.sqlsession.SqlSession;
import com.mini.mybatis.mybatisministep15.sqlsession.SqlSessionFactory;
import com.mini.mybatis.mybatisministep15.sqlsession.SqlSessionFactoryBuilder;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

@SpringBootTest
class MybatisMiniStep15ApplicationTests {

    private static SqlSession sqlSession;

    @Test
    void contextLoads() {
    }


    @BeforeAll
    public static void init() throws IOException {
        // 首先通过配置文件获取sqlSessionFactory
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(Resources.getResourceAsReader("mybatis-config.xml"));
        // 获取sqlSession
        sqlSession = sqlSessionFactory.openSession();
    }


    @Test
    public void test() throws IOException {
        // 首先通过配置文件获取sqlSessionFactory
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(Resources.getResourceAsReader("mybatis-config.xml"));
        // 获取sqlSession
        SqlSession sqlSession = sqlSessionFactory.openSession();
        // 获取指定Mapper的代理
        TeamMapper teamMapper = sqlSession.getMapper(TeamMapper.class);
        // 执行代理对象的查询方法
        Team team = teamMapper.selectByTeamId(1006);
        System.out.println(team);
    }

    @Test
    void testReflection(){
        Team team = new Team();
        MetaObject metaObject = SystemMetaObject.forObject(team);
        String[] getterNames = metaObject.getGetterNames();
        System.out.println(Arrays.toString(getterNames));
        String[] setterNames = metaObject.getSetterNames();
        System.out.println(Arrays.toString(setterNames));
        metaObject.setValue("team_type",55);
        System.out.println(team);

    }

    @Test
    public void test3(){
        // 通过名字获取的，sql中是resultType而不是resultMap，之所以映射不上是因为，数据库是team_id这种格式，而Team类中是teamId驼峰格式，
        // 通过反射去找set方法的时候这里没有做具体的处理，所以找不到对应的set方法，查询结果就是Team{teamId=null, teamName='null', teamType=null}
        TeamMapper mapper = sqlSession.getMapper(TeamMapper.class);
        Team team = mapper.selectByTeamName("阿根廷");
        System.out.println(team);
    }

    @Test
    public void test4(){
        TeamMapper mapper = sqlSession.getMapper(TeamMapper.class);
        List<Team> teams = mapper.selectAllTeam();
        System.out.println(teams);
    }


    @Test
    public void testInsert(){
        TeamMapper mapper = sqlSession.getMapper(TeamMapper.class);
        Team team = new Team();
//        team.setTeamId(1010);
        team.setTeamName("荷兰");
        team.setTeamType(10);
        mapper.insert(team);
        // 3. 提交事务
        sqlSession.commit();
        System.out.println(team);
    }

    @Test
    public void testUpdate(){
        TeamMapper mapper = sqlSession.getMapper(TeamMapper.class);
        Team team = new Team();
        team.setTeamType(1004);
        team.setTeamName("克罗地亚");
        team.setTeamType(4);
        mapper.update(team);
        // 3. 提交事务
        sqlSession.commit();
        System.out.println(team);
    }

    @Test
    public void testDelete(){
        TeamMapper mapper = sqlSession.getMapper(TeamMapper.class);
        mapper.delete(1010);
        // 3. 提交事务
        sqlSession.commit();
    }

    @Test
    void testAnno(){
        TeamAnnoMapper teamAnnoMapper = sqlSession.getMapper(TeamAnnoMapper.class);
        Team team = teamAnnoMapper.selectByAnno(1001);
        System.out.println(team);
    }


    @Test
    void testDynamic(){
        TeamMapper teamMapper = sqlSession.getMapper(TeamMapper.class);
        Team team = new Team();
        team.setTeamId(1004);
        team.setTeamName("克罗地亚");
//        team.setTeamType(4);
        List<Team> teams = teamMapper.selectByTeamIdDyn(team);
        System.out.println(teams);
    }


}
