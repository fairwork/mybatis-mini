package com.mini.mybatis.mybatisministep01;

import com.mini.mybatis.mybatisministep01.binding.MapperProxyFactory;
import com.mini.mybatis.mybatisministep01.mapper.IUserMapper;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.HashMap;

@SpringBootTest
class MybatisMiniStep01ApplicationTests {

	@Test
	void contextLoads() {
	}


	@Test
	public void test_mapperProxyFactory(){
		MapperProxyFactory<IUserMapper> mapperProxyFactory = new MapperProxyFactory<>(IUserMapper.class);

		HashMap<String, Object> sqlSession = new HashMap<>();
		sqlSession.put("IUserMapper.selectUserNameByUserId","执行通过userId查询username的操作");
		sqlSession.put("IUserMapper.selectUserIdByUsername","执行通过username查询userId的操作");

		IUserMapper userMapper = mapperProxyFactory.newInstance(sqlSession);
		String username = userMapper.selectUserNameByUserId(111);
		System.out.println(username);
		String userId = userMapper.selectUserIdByUsername("harvey");
		System.out.println(userId);

	}

}
