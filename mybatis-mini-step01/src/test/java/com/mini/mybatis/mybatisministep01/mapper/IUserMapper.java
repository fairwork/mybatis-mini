package com.mini.mybatis.mybatisministep01.mapper;

public interface IUserMapper {

    String selectUserNameByUserId(Integer userId);

    String selectUserIdByUsername(String username);

}
