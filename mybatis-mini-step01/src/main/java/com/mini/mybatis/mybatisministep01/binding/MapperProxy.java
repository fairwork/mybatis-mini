package com.mini.mybatis.mybatisministep01.binding;

import java.io.Serializable;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

/**
 *  1.当我们来设计一个ORM 框架的过程中，首先要考虑怎么把用户定义的数据库操作接口、xml配置的SQL语句、数据库三者联系起来。
 *      其实最适合的操作就是  使用代理的方式  进行处理，因为代理可以封装一个复杂的流程为接口对象的实现类
 *  2.MapperProxy 负责实现 InvocationHandler 接口的 invoke 方法，最终所有的实际调用都会调用到这个方法包装的逻辑。
 */
public class MapperProxy<T> implements InvocationHandler, Serializable {

    private Map<String,Object> sqlSession;

    // final修饰的属性只能被初始化赋值一次
    private final Class<T> mapperInterface;


    public MapperProxy(Map<String,Object> sqlSession, Class<T> mapperInterface){
        this.sqlSession = sqlSession;
        this.mapperInterface = mapperInterface;
    }


    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        if (Object.class.equals(method.getDeclaringClass())){
            return method.invoke(this,args);
        } else {
            // 执行代理增强逻辑 模拟执行sql语句
            return sqlSession.get(mapperInterface.getSimpleName() + "." + method.getName());
        }
    }
}
