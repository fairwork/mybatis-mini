package com.mini.mybatis.mybatisministep01;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MybatisMiniStep01Application {

	public static void main(String[] args) {
		SpringApplication.run(MybatisMiniStep01Application.class, args);
	}

}
