package com.mini.mybatis.mybatisministep11.mapping;

public interface SqlSource {

    BoundSql getBoundSql(Object parameterObject);

}
