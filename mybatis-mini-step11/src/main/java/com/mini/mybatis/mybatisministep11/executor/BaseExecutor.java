package com.mini.mybatis.mybatisministep11.executor;

import com.mini.mybatis.mybatisministep11.mapping.BoundSql;
import com.mini.mybatis.mybatisministep11.mapping.MappedStatement;
import com.mini.mybatis.mybatisministep11.sqlsession.ResultHandler;
import com.mini.mybatis.mybatisministep11.sqlsession.RowBounds;
import com.mini.mybatis.mybatisministep11.sqlsession.configuration.Configuration;
import com.mini.mybatis.mybatisministep11.transaction.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

/**
 * 执行器抽象基类，定义步骤模板
 */
public abstract class BaseExecutor implements Executor{

    private Logger logger = LoggerFactory.getLogger(BaseExecutor.class);

    protected Configuration configuration;

    protected Transaction transaction;

    protected Executor wrapper;

    private boolean closed = false;


    public BaseExecutor(Configuration configuration, Transaction transaction) {
        this.configuration = configuration;
        this.transaction = transaction;
        this.wrapper = this;
    }

    @Override
    public int update(MappedStatement ms, Object parameter) throws SQLException {
        return doUpdate(ms, parameter);
    }

    @Override
    public <E> List<E> query(MappedStatement ms, Object parameter, RowBounds rowBounds, ResultHandler resultHandler, BoundSql boundSql) throws SQLException {
        if (closed){
            throw new RuntimeException("Executor was close!");
        }
        return doQuery(ms,parameter,rowBounds,resultHandler,boundSql);
    }

    protected abstract int doUpdate(MappedStatement ms, Object parameter) throws SQLException;


    protected abstract <E> List<E> doQuery(MappedStatement ms, Object parameter, RowBounds rowBounds, ResultHandler resultHandler, BoundSql boundSql) throws SQLException;

    @Override
    public Transaction getTransaction() {
        if (closed){
            throw new RuntimeException("Executor was close!");
        }
        return transaction;
    }

    @Override
    public void commit(boolean require) throws SQLException {
        if (closed){
            throw new RuntimeException("Executor was close!");
        }
        if (require){
            transaction.commit();
        }
    }

    @Override
    public void rollback(boolean require) throws SQLException {
        if (!closed){
            if (require){
                transaction.rollback();
            }
        }
    }

    @Override
    public void close(boolean forceRollback) {
        try {
            try {
                rollback(forceRollback);
            } finally {
                transaction.close();
            }
        } catch (SQLException e) {
            logger.warn("Unexpected exception on closing transaction.  Cause: " + e);
        } finally {
            transaction = null;
            closed = true;
        }
    }

    protected void closeStatement(Statement statement) {
        if (statement != null) {
            try {
                statement.close();
            } catch (SQLException ignore) {
            }
        }
    }
}
