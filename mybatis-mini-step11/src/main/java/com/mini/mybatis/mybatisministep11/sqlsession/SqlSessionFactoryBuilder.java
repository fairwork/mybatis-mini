package com.mini.mybatis.mybatisministep11.sqlsession;

import com.mini.mybatis.mybatisministep11.builder.xml.XmlConfigBuilder;
import com.mini.mybatis.mybatisministep11.sqlsession.configuration.Configuration;
import com.mini.mybatis.mybatisministep11.sqlsession.defaults.DefaultSqlSessionFactory;

import java.io.Reader;

/**
 * MyBatis的入口
 */
public class SqlSessionFactoryBuilder {


    public SqlSessionFactory build(Reader reader){
        XmlConfigBuilder xmlConfigBuilder = new XmlConfigBuilder(reader);
        return build(xmlConfigBuilder.parse());
    }

    public SqlSessionFactory build(Configuration configuration){
        return new DefaultSqlSessionFactory(configuration);
    }


}
