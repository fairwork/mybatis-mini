package com.mini.mybatis.mybatisministep11.reflection;


import com.mini.mybatis.mybatisministep11.reflection.factory.DefaultObjectFactory;
import com.mini.mybatis.mybatisministep11.reflection.factory.ObjectFactory;
import com.mini.mybatis.mybatisministep11.reflection.wrapper.DefaultObjectWrapperFactory;
import com.mini.mybatis.mybatisministep11.reflection.wrapper.ObjectWrapperFactory;

/**
 *  该类相当于一个最顶层获取反射器的入口，统一封装对象工厂、对象包装工厂，通过forObject(obj) 方法去获得对应object 的 MetaObject，
 *  相当于MetaObject的封装，让外界更轻易调用，从而获取MetaObject
 */
public class SystemMetaObject {

    // 对象工厂 - 统一创建对象的工厂
    public static final ObjectFactory DEFAULT_OBJECT_FACTORY = new DefaultObjectFactory();
    // 对象包装工厂 - 用于判断当前对象是否已经被包装，还可获得包装器
    public static final ObjectWrapperFactory DEFAULT_OBJECT_WRAPPER_FACTORY = new DefaultObjectWrapperFactory();
    // 空对象
    public static final MetaObject NULL_META_OBJECT = MetaObject.forObject(NullObject.class, DEFAULT_OBJECT_FACTORY, DEFAULT_OBJECT_WRAPPER_FACTORY);

    private SystemMetaObject() {
        // Prevent Instantiation of Static Class
    }

    /**
     * 空对象
     */
    private static class NullObject {
    }

    public static MetaObject forObject(Object object) {
        return MetaObject.forObject(object, DEFAULT_OBJECT_FACTORY, DEFAULT_OBJECT_WRAPPER_FACTORY);
    }

}
