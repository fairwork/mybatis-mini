package com.mini.mybatis.mybatisministep11;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MybatisMiniStep11Application {

    public static void main(String[] args) {
        SpringApplication.run(MybatisMiniStep11Application.class, args);
    }

}
