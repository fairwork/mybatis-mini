package com.mini.mybatis.mybatisministep11.builder;


import com.mini.mybatis.mybatisministep11.mapping.BoundSql;
import com.mini.mybatis.mybatisministep11.mapping.ParameterMapping;
import com.mini.mybatis.mybatisministep11.mapping.SqlSource;
import com.mini.mybatis.mybatisministep11.sqlsession.configuration.Configuration;

import java.util.List;


public class StaticSqlSource implements SqlSource {

    private String sql;
    private List<ParameterMapping> parameterMappings;
    private Configuration configuration;

    public StaticSqlSource(Configuration configuration, String sql) {
        this(configuration, sql, null);
    }

    public StaticSqlSource(Configuration configuration, String sql, List<ParameterMapping> parameterMappings) {
        this.sql = sql;
        this.parameterMappings = parameterMappings;
        this.configuration = configuration;
    }

    @Override
    public BoundSql getBoundSql(Object parameterObject) {
        return new BoundSql(configuration, sql, parameterMappings, parameterObject);
    }

}
