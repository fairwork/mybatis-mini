package com.mini.mybatis.mybatisministep11.mapping;


import com.mini.mybatis.mybatisministep11.sqlsession.configuration.Configuration;
import com.mini.mybatis.mybatisministep11.type.JdbcType;
import com.mini.mybatis.mybatisministep11.type.TypeHandler;


public class ResultMapping {

    private Configuration configuration;
    private String property;
    private String column;
    private Class<?> javaType;
    private JdbcType jdbcType;
    private TypeHandler<?> typeHandler;

    ResultMapping() {
    }

    public static class Builder {
        private ResultMapping resultMapping = new ResultMapping();


    }

}
