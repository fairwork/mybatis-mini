package com.mini.mybatis.mybatisministep11.scripting.xmltags;


import com.mini.mybatis.mybatisministep11.executor.parameter.ParameterHandler;
import com.mini.mybatis.mybatisministep11.mapping.BoundSql;
import com.mini.mybatis.mybatisministep11.mapping.MappedStatement;
import com.mini.mybatis.mybatisministep11.mapping.SqlSource;
import com.mini.mybatis.mybatisministep11.scripting.LanguageDriver;
import com.mini.mybatis.mybatisministep11.scripting.defaults.DefaultParameterHandler;
import com.mini.mybatis.mybatisministep11.sqlsession.configuration.Configuration;
import org.dom4j.Element;


public class XMLLanguageDriver implements LanguageDriver {

    @Override
    public SqlSource createSqlSource(Configuration configuration, Element script, Class<?> parameterType) {
        // 用XML脚本构建器解析sql
        XMLScriptBuilder builder = new XMLScriptBuilder(configuration, script, parameterType);
        return builder.parseScriptNode();
    }

    @Override
    public ParameterHandler createParameterHandler(MappedStatement mappedStatement, Object parameterObject, BoundSql boundSql) {
        return new DefaultParameterHandler(mappedStatement, parameterObject, boundSql);
    }

}