package com.mini.mybatis.mybatisministep11.sqlsession;

public interface ResultHandler {

    void handleResult(ResultContext context);

}
