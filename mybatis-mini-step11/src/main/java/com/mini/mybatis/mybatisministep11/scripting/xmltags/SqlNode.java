package com.mini.mybatis.mybatisministep11.scripting.xmltags;


public interface SqlNode {

    boolean apply(DynamicContext context);

}