package com.mini.mybatis.mybatisministep11.parsing;


public interface TokenHandler {

    String handleToken(String content);

}
