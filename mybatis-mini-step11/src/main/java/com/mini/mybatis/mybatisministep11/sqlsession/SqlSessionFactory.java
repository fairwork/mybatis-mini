package com.mini.mybatis.mybatisministep11.sqlsession;

public interface SqlSessionFactory {

    SqlSession openSession();

}
