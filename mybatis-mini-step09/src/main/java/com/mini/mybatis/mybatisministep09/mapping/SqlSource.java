package com.mini.mybatis.mybatisministep09.mapping;

public interface SqlSource {

    BoundSql getBoundSql(Object parameterObject);

}
