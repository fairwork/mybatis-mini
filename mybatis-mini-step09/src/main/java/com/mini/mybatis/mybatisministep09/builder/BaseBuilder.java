package com.mini.mybatis.mybatisministep09.builder;

import com.mini.mybatis.mybatisministep09.sqlsession.configuration.Configuration;
import com.mini.mybatis.mybatisministep09.type.TypeAliasRegistry;
import com.mini.mybatis.mybatisministep09.type.TypeHandlerRegistry;

public class BaseBuilder {

    protected Configuration configuration;

    protected TypeAliasRegistry typeAliasRegistry;

    protected TypeHandlerRegistry typeHandlerRegistry;

    public BaseBuilder(Configuration configuration) {
        this.configuration = configuration;
        typeAliasRegistry = this.configuration.getTypeAliasRegistry();
        typeHandlerRegistry = this.configuration.getTypeHandlerRegistry();
    }

    public Configuration getConfiguration(){
        return configuration;
    }

    public Class<?> resolveAlias(String alias){
        return typeAliasRegistry.resolveAlias(alias);
    }
}
