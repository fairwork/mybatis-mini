package com.mini.mybatis.mybatisministep09.scripting.xmltags;


public interface SqlNode {

    boolean apply(DynamicContext context);

}