package com.mini.mybatis.mybatisministep09.sqlsession;

public interface SqlSessionFactory {

    SqlSession openSession();

}
