package com.mini.mybatis.mybatisministep09.parsing;


public interface TokenHandler {

    String handleToken(String content);

}
