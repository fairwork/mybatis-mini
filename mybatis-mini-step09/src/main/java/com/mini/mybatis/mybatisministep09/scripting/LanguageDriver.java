package com.mini.mybatis.mybatisministep09.scripting;


import com.mini.mybatis.mybatisministep09.executor.parameter.ParameterHandler;
import com.mini.mybatis.mybatisministep09.mapping.BoundSql;
import com.mini.mybatis.mybatisministep09.mapping.MappedStatement;
import com.mini.mybatis.mybatisministep09.mapping.SqlSource;
import com.mini.mybatis.mybatisministep09.sqlsession.configuration.Configuration;
import org.dom4j.Element;


public interface LanguageDriver {

    /**
     * 创建SQL源码(mapper xml方式)
     */
    SqlSource createSqlSource(Configuration configuration, Element script, Class<?> parameterType);

    /**
     * 创建参数处理器
     */
    ParameterHandler createParameterHandler(MappedStatement mappedStatement, Object parameterObject, BoundSql boundSql);


}
