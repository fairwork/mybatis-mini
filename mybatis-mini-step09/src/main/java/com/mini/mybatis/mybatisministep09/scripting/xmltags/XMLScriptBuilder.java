package com.mini.mybatis.mybatisministep09.scripting.xmltags;


import com.mini.mybatis.mybatisministep09.builder.BaseBuilder;
import com.mini.mybatis.mybatisministep09.mapping.SqlSource;
import com.mini.mybatis.mybatisministep09.scripting.defaults.RawSqlSource;
import com.mini.mybatis.mybatisministep09.sqlsession.configuration.Configuration;
import org.dom4j.Element;

import java.util.ArrayList;
import java.util.List;


public class XMLScriptBuilder extends BaseBuilder {

    private Element element;
    private boolean isDynamic;
    private Class<?> parameterType;

    public XMLScriptBuilder(Configuration configuration, Element element, Class<?> parameterType) {
        super(configuration);
        this.element = element;
        this.parameterType = parameterType;
    }

    public SqlSource parseScriptNode() {
        List<SqlNode> contents = parseDynamicTags(element);
        MixedSqlNode rootSqlNode = new MixedSqlNode(contents);
        return new RawSqlSource(configuration, rootSqlNode, parameterType);
    }

    List<SqlNode> parseDynamicTags(Element element) {
        List<SqlNode> contents = new ArrayList<>();
        // element.getText 拿到 SQL
        String data = element.getText();
        contents.add(new StaticTextSqlNode(data));
        return contents;
    }

}
