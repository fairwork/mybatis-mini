package com.mini.mybatis.mybatisministep09;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MybatisMiniStep09Application {

    public static void main(String[] args) {
        SpringApplication.run(MybatisMiniStep09Application.class, args);
    }

}
