package com.mini.mybatis.mybatisministep09.type;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public class LongTypeHandler extends BaseTypeHandler<Long>{


    @Override
    protected void setNoNullParameter(PreparedStatement ps, int i, Long parameter, JdbcType jdbcType) throws SQLException {
        ps.setLong(i,parameter);
    }
}
