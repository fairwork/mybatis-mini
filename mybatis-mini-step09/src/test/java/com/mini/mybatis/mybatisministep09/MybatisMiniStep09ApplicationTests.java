package com.mini.mybatis.mybatisministep09;

import com.mini.mybatis.mybatisministep09.entity.Team;
import com.mini.mybatis.mybatisministep09.io.Resources;
import com.mini.mybatis.mybatisministep09.mapper.TeamMapper;
import com.mini.mybatis.mybatisministep09.reflection.MetaObject;
import com.mini.mybatis.mybatisministep09.reflection.SystemMetaObject;
import com.mini.mybatis.mybatisministep09.sqlsession.SqlSession;
import com.mini.mybatis.mybatisministep09.sqlsession.SqlSessionFactory;
import com.mini.mybatis.mybatisministep09.sqlsession.SqlSessionFactoryBuilder;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;
import java.util.Arrays;

@SpringBootTest
class MybatisMiniStep09ApplicationTests {

    private static SqlSession sqlSession;

    @Test
    void contextLoads() {
    }


    @BeforeAll
    public static void init() throws IOException {
        // 首先通过配置文件获取sqlSessionFactory
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(Resources.getResourceAsReader("mybatis-config.xml"));
        // 获取sqlSession
        sqlSession = sqlSessionFactory.openSession();
    }


    @Test
    public void test() throws IOException {
        // 首先通过配置文件获取sqlSessionFactory
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(Resources.getResourceAsReader("mybatis-config.xml"));
        // 获取sqlSession
        SqlSession sqlSession = sqlSessionFactory.openSession();
        // 获取指定Mapper的代理
        TeamMapper teamMapper = sqlSession.getMapper(TeamMapper.class);
        // 执行代理对象的查询方法
        Team team = teamMapper.selectByTeamId(1006);
        System.out.println(team);
    }

    @Test
    void testReflection(){
        Team team = new Team();
        MetaObject metaObject = SystemMetaObject.forObject(team);
        String[] getterNames = metaObject.getGetterNames();
        System.out.println(Arrays.toString(getterNames));
        String[] setterNames = metaObject.getSetterNames();
        System.out.println(Arrays.toString(setterNames));
        metaObject.setValue("team_type",55);
        System.out.println(team);

    }

    @Test
    public void test3(){
        TeamMapper mapper = sqlSession.getMapper(TeamMapper.class);
        Team team = mapper.selectByTeamName("阿根廷");
        System.out.println(team);
    }
}
