package com.mini.mybatis.mybatisministep09.mapper;

import com.mini.mybatis.mybatisministep09.entity.Team;

public interface TeamMapper {


    Team selectByTeamId(Integer teamId);

    Team selectByTeamName(String teamName);

}
