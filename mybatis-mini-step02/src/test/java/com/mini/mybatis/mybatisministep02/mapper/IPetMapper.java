package com.mini.mybatis.mybatisministep02.mapper;

public interface IPetMapper {

    String getPetNameById(Integer petId);

}
