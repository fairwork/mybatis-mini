package com.mini.mybatis.mybatisministep02;

import com.mini.mybatis.mybatisministep02.binding.MapperRegistry;
import com.mini.mybatis.mybatisministep02.mapper.IPetMapper;
import com.mini.mybatis.mybatisministep02.mapper.IUserMapper;
import com.mini.mybatis.mybatisministep02.sqlsession.SqlSession;
import com.mini.mybatis.mybatisministep02.sqlsession.SqlSessionFactory;
import com.mini.mybatis.mybatisministep02.sqlsession.defaults.DefaultSqlSessionFactory;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class MybatisMiniStep02ApplicationTests {

    @Test
    void contextLoads() {
    }


    @Test
    public void sqlSessionTest(){

        // 通过扫包注册mapper 这一步在后续是需要在启动的时候，通过扫包注册到容器中
        MapperRegistry mapperRegistry = new MapperRegistry();
        mapperRegistry.addMappers("com.mini.mybatis.mybatisministep02.mapper");

        // 正式源码这步是在配置类或配置文件中配置SqlSessionFactory
        SqlSessionFactory sqlSessionFactory = new DefaultSqlSessionFactory(mapperRegistry);

        // 正式源码中这步是SqlSessionTemplate中的 代理sqlSessionProxy 的SqlSessionInterceptor的invoke方法开启的
        SqlSession sqlSession = sqlSessionFactory.openSession();

        // 正式源码这步是在属性注入mapper的时候从工厂的getObject中获取mapper代理对象
        IUserMapper userMapper = sqlSession.getMapper(IUserMapper.class);

        // --------以上步骤需要框架完成，也是我们底层需要屏蔽的操作------------

        String username = userMapper.selectUserNameByUserId(1001);
        System.out.println(username);

        IPetMapper petMapper = sqlSession.getMapper(IPetMapper.class);
        String petName = petMapper.getPetNameById(2002);
        System.out.println(petName);

    }

}
