package com.mini.mybatis.mybatisministep02.mapper;

public interface IUserMapper {

    String selectUserNameByUserId(Integer userId);

    String selectUserIdByUsername(String username);

}
