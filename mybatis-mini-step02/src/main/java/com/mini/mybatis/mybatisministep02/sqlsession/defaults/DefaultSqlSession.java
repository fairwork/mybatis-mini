package com.mini.mybatis.mybatisministep02.sqlsession.defaults;

import com.mini.mybatis.mybatisministep02.binding.MapperRegistry;
import com.mini.mybatis.mybatisministep02.sqlsession.SqlSession;

public class DefaultSqlSession implements SqlSession {

    private MapperRegistry mapperRegistry;

    public DefaultSqlSession(MapperRegistry mapperRegistry){
        this.mapperRegistry = mapperRegistry;
    }

    @Override
    public <T> T selectOne(String statement) {
        return (T) ("被代理了 " + "statement" + statement);
    }

    @Override
    public <T> T selectOne(String statement, Object parameter) {
        return (T) ("被代理了" + " statement" + statement + " parameter" + parameter);
    }

    @Override
    public <T> T getMapper(Class<T> type) {
        return mapperRegistry.getMapper(type,this);
    }
}
