package com.mini.mybatis.mybatisministep02.sqlsession;

public interface SqlSessionFactory {

    SqlSession openSession();
}
