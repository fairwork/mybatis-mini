package com.mini.mybatis.mybatisministep02;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MybatisMiniStep02Application {

    public static void main(String[] args) {
        SpringApplication.run(MybatisMiniStep02Application.class, args);
    }

}
