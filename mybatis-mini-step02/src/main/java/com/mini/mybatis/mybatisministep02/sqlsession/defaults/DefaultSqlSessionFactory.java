package com.mini.mybatis.mybatisministep02.sqlsession.defaults;

import com.mini.mybatis.mybatisministep02.binding.MapperRegistry;
import com.mini.mybatis.mybatisministep02.sqlsession.SqlSession;
import com.mini.mybatis.mybatisministep02.sqlsession.SqlSessionFactory;

public class DefaultSqlSessionFactory implements SqlSessionFactory {

    private MapperRegistry mapperRegistry;

    public DefaultSqlSessionFactory(MapperRegistry mapperRegistry) {
        this.mapperRegistry = mapperRegistry;
    }

    @Override
    public SqlSession openSession() {
        return new DefaultSqlSession(mapperRegistry);
    }
}
