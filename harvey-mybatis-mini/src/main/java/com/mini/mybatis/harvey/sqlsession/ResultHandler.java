package com.mini.mybatis.harvey.sqlsession;

public interface ResultHandler {

    void handleResult(ResultContext context);

}
