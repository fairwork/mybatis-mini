package com.mini.mybatis.harvey.scripting;


import com.mini.mybatis.harvey.executor.parameter.ParameterHandler;
import com.mini.mybatis.harvey.mapping.MappedStatement;
import com.mini.mybatis.harvey.mapping.BoundSql;
import com.mini.mybatis.harvey.mapping.SqlSource;
import com.mini.mybatis.harvey.sqlsession.configuration.Configuration;
import org.dom4j.Element;


public interface LanguageDriver {

    /**
     * 创建SQL源码(mapper xml方式)
     */
    SqlSource createSqlSource(Configuration configuration, Element script, Class<?> parameterType);

    /**
     * 创建SQL源码(annotation 注解方式)
     */
    SqlSource createSqlSource(Configuration configuration, String script, Class<?> parameterType);

    /**
     * 创建参数处理器
     */
    ParameterHandler createParameterHandler(MappedStatement mappedStatement, Object parameterObject, BoundSql boundSql);


}
