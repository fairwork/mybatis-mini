package com.mini.mybatis.harvey.sqlsession;

public interface SqlSessionFactory {

    SqlSession openSession();

}
