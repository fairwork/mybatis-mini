package com.mini.mybatis.harvey.scripting.xmltags;


public interface SqlNode {

    boolean apply(DynamicContext context);

}