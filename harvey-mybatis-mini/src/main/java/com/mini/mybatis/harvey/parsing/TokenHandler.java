package com.mini.mybatis.harvey.parsing;


public interface TokenHandler {

    String handleToken(String content);

}
