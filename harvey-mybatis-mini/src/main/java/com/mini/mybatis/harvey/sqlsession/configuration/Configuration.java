package com.mini.mybatis.harvey.sqlsession.configuration;

import com.mini.mybatis.harvey.cache.decorators.FifoCache;
import com.mini.mybatis.harvey.cache.impl.PerpetualCache;
import com.mini.mybatis.harvey.datasource.durid.DruidDataSourceFactory;
import com.mini.mybatis.harvey.datasource.pooled.PooledDataSourceFactory;
import com.mini.mybatis.harvey.datasource.unpooled.UnPooledDataSourceFactory;
import com.mini.mybatis.harvey.executor.CachingExecutor;
import com.mini.mybatis.harvey.executor.Executor;
import com.mini.mybatis.harvey.executor.SimpleExecutor;
import com.mini.mybatis.harvey.executor.keygen.KeyGenerator;
import com.mini.mybatis.harvey.executor.parameter.ParameterHandler;
import com.mini.mybatis.harvey.executor.resultset.DefaultResultSetHandler;
import com.mini.mybatis.harvey.executor.resultset.ResultSetHandler;
import com.mini.mybatis.harvey.executor.statement.PrepareStatementHandler;
import com.mini.mybatis.harvey.executor.statement.StatementHandler;
import com.mini.mybatis.harvey.mapping.Environment;
import com.mini.mybatis.harvey.mapping.MappedStatement;
import com.mini.mybatis.harvey.reflection.MetaObject;
import com.mini.mybatis.harvey.reflection.factory.DefaultObjectFactory;
import com.mini.mybatis.harvey.reflection.factory.ObjectFactory;
import com.mini.mybatis.harvey.reflection.wrapper.DefaultObjectWrapperFactory;
import com.mini.mybatis.harvey.reflection.wrapper.ObjectWrapperFactory;
import com.mini.mybatis.harvey.scripting.LanguageDriver;
import com.mini.mybatis.harvey.scripting.LanguageDriverRegistry;
import com.mini.mybatis.harvey.scripting.xmltags.XMLLanguageDriver;
import com.mini.mybatis.harvey.sqlsession.RowBounds;
import com.mini.mybatis.harvey.sqlsession.SqlSession;
import com.mini.mybatis.harvey.type.TypeAliasRegistry;
import com.mini.mybatis.harvey.binding.MapperRegistry;
import com.mini.mybatis.harvey.cache.Cache;
import com.mini.mybatis.harvey.mapping.BoundSql;
import com.mini.mybatis.harvey.mapping.ResultMap;
import com.mini.mybatis.harvey.plugin.Interceptor;
import com.mini.mybatis.harvey.plugin.InterceptorChain;
import com.mini.mybatis.harvey.sqlsession.LocalCacheScope;
import com.mini.mybatis.harvey.sqlsession.ResultHandler;
import com.mini.mybatis.harvey.transaction.Transaction;
import com.mini.mybatis.harvey.transaction.jdbc.JdbcTransactionFactory;
import com.mini.mybatis.harvey.type.TypeHandlerRegistry;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * MyBatis底层全局配置
 */
public class Configuration {

    protected Environment environment;

    protected boolean useGeneratedKeys = false;

    // 默认启用缓存，cacheEnabled = true/false
    protected boolean cacheEnabled = true;

    // 缓存机制，默认不配置的情况是 SESSION
    protected LocalCacheScope localCacheScope = LocalCacheScope.SESSION;

    // 映射注册机
    protected MapperRegistry mapperRegistry = new MapperRegistry(this);

    // 映射的语句，存在Map里
    protected final Map<String, MappedStatement> mappedStatements = new HashMap<>();
    // 缓存,存在Map里
    protected final Map<String, Cache> caches = new HashMap<>();
    // 结果映射，存在Map里
    protected final Map<String, ResultMap> resultMaps = new HashMap<>();

    protected final Map<String, KeyGenerator> keyGenerators = new HashMap<>();

    // 插件拦截器链
    protected final InterceptorChain interceptorChain = new InterceptorChain();


    // 类型别名注册机
    protected final TypeAliasRegistry typeAliasRegistry = new TypeAliasRegistry();
    protected final LanguageDriverRegistry languageRegistry = new LanguageDriverRegistry();

    // 类型处理器注册机
    protected final TypeHandlerRegistry typeHandlerRegistry = new TypeHandlerRegistry();

    // 对象工厂和对象包装器工厂
    protected ObjectFactory objectFactory = new DefaultObjectFactory();
    protected ObjectWrapperFactory objectWrapperFactory = new DefaultObjectWrapperFactory();

    protected final Set<String> loadedResources = new HashSet<>();

    protected String databaseId;

    public Configuration() {
        typeAliasRegistry.registerAlias("JDBC", JdbcTransactionFactory.class);

        typeAliasRegistry.registerAlias("DRUID", DruidDataSourceFactory.class);
        typeAliasRegistry.registerAlias("UNPOOLED", UnPooledDataSourceFactory.class);
        typeAliasRegistry.registerAlias("POOLED", PooledDataSourceFactory.class);

        typeAliasRegistry.registerAlias("PERPETUAL", PerpetualCache.class);
        typeAliasRegistry.registerAlias("FIFO", FifoCache.class);

        languageRegistry.setDefaultDriverClass(XMLLanguageDriver.class);
    }

    public void addMappers(String packageName) {
        mapperRegistry.addMappers(packageName);
    }

    public <T> void addMapper(Class<T> type) {
        mapperRegistry.addMapper(type);
    }

    public <T> T getMapper(Class<T> type, SqlSession sqlSession) {
        return mapperRegistry.getMapper(type, sqlSession);
    }

    public boolean hasMapper(Class<?> type) {
        return mapperRegistry.hasMapper(type);
    }

    public void addMappedStatement(MappedStatement ms) {
        mappedStatements.put(ms.getId(), ms);
    }

    public MappedStatement getMappedStatement(String id) {
        return mappedStatements.get(id);
    }

    public TypeAliasRegistry getTypeAliasRegistry() {
        return typeAliasRegistry;
    }

    public Environment getEnvironment() {
        return environment;
    }

    public void setEnvironment(Environment environment) {
        this.environment = environment;
    }

    public String getDatabaseId() {
        return databaseId;
    }

    /**
     * 创建结果集处理器
     */
    public ResultSetHandler newResultSetHandler(Executor executor, MappedStatement mappedStatement, RowBounds rowBounds, ResultHandler resultHandler, BoundSql boundSql) {
        ResultSetHandler resultSetHandler = new DefaultResultSetHandler(executor, mappedStatement, resultHandler, rowBounds, boundSql);
        resultSetHandler = (ResultSetHandler) interceptorChain.pluginAll(resultSetHandler);
        return resultSetHandler;
    }

    /**
     * 生产执行器
     */
    public Executor newExecutor(Transaction transaction) {
        Executor executor = new SimpleExecutor(this, transaction);
        // 配置开启缓存，创建 CachingExecutor(默认就是有缓存)装饰者模式
        if (cacheEnabled) {
            executor = new CachingExecutor(executor);
        }
        executor = (Executor) interceptorChain.pluginAll(executor);
        return executor;
    }

    /**
     * 创建语句处理器
     */
    public StatementHandler newStatementHandler(Executor executor, MappedStatement mappedStatement, Object parameter, RowBounds rowBounds, ResultHandler resultHandler, BoundSql boundSql) {
        StatementHandler statementHandler = new PrepareStatementHandler(executor, mappedStatement, parameter, rowBounds, resultHandler, boundSql);
        statementHandler = (StatementHandler) interceptorChain.pluginAll(statementHandler);
        return statementHandler;
    }

    // 创建元对象
    public MetaObject newMetaObject(Object object) {
        return MetaObject.forObject(object, objectFactory, objectWrapperFactory);
    }

    // 类型处理器注册机
    public TypeHandlerRegistry getTypeHandlerRegistry() {
        return typeHandlerRegistry;
    }

    public boolean isResourceLoaded(String resource) {
        return loadedResources.contains(resource);
    }

    public void addLoadedResource(String resource) {
        loadedResources.add(resource);
    }

    public LanguageDriverRegistry getLanguageRegistry() {
        return languageRegistry;
    }

    public ParameterHandler newParameterHandler(MappedStatement mappedStatement, Object parameterObject, BoundSql boundSql) {
        // 创建参数处理器
        ParameterHandler parameterHandler = mappedStatement.getLang().createParameterHandler(mappedStatement, parameterObject, boundSql);
        // 插件的一些参数，也是在这里处理，interceptorChain.pluginAll(parameterHandler);
        parameterHandler = (ParameterHandler) interceptorChain.pluginAll(parameterHandler);
        return parameterHandler;
    }

    public LanguageDriver getDefaultScriptingLanguageInstance() {
        return languageRegistry.getDefaultDriver();
    }

    public ObjectFactory getObjectFactory() {
        return objectFactory;
    }

    public ResultMap getResultMap(String id) {
        return resultMaps.get(id);
    }
    public void addResultMap(ResultMap resultMap) {
        resultMaps.put(resultMap.getId(), resultMap);
    }

    public void addKeyGenerator(String id, KeyGenerator keyGenerator) {
        keyGenerators.put(id, keyGenerator);
    }

    public KeyGenerator getKeyGenerator(String id) {
        return keyGenerators.get(id);
    }

    public boolean hasKeyGenerator(String id) {
        return keyGenerators.containsKey(id);
    }

    public boolean isUseGeneratedKeys() {
        return useGeneratedKeys;
    }

    public void setUseGeneratedKeys(boolean useGeneratedKeys) {
        this.useGeneratedKeys = useGeneratedKeys;
    }

    public void addInterceptor(Interceptor interceptorInstance) {
        interceptorChain.addInterceptor(interceptorInstance);
    }

    public LocalCacheScope getLocalCacheScope() {
        return localCacheScope;
    }

    public void setLocalCacheScope(LocalCacheScope localCacheScope) {
        this.localCacheScope = localCacheScope;
    }
    public boolean isCacheEnabled() {
        return cacheEnabled;
    }

    public void setCacheEnabled(boolean cacheEnabled) {
        this.cacheEnabled = cacheEnabled;
    }

    public void addCache(Cache cache) {
        caches.put(cache.getId(), cache);
    }

    public Cache getCache(String id) {
        return caches.get(id);
    }
}
