package com.mini.mybatis.harvey.builder;


import com.mini.mybatis.harvey.mapping.BoundSql;
import com.mini.mybatis.harvey.mapping.ParameterMapping;
import com.mini.mybatis.harvey.mapping.SqlSource;
import com.mini.mybatis.harvey.sqlsession.configuration.Configuration;

import java.util.List;


public class StaticSqlSource implements SqlSource {

    private String sql;
    private List<ParameterMapping> parameterMappings;
    private Configuration configuration;

    public StaticSqlSource(Configuration configuration, String sql) {
        this(configuration, sql, null);
    }

    public StaticSqlSource(Configuration configuration, String sql, List<ParameterMapping> parameterMappings) {
        this.sql = sql;
        this.parameterMappings = parameterMappings;
        this.configuration = configuration;
    }

    @Override
    public BoundSql getBoundSql(Object parameterObject) {
        return new BoundSql(configuration, sql, parameterMappings, parameterObject);
    }

}
