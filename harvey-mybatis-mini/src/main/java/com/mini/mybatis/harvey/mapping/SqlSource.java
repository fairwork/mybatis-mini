package com.mini.mybatis.harvey.mapping;

public interface SqlSource {

    BoundSql getBoundSql(Object parameterObject);

}
