package com.mini.mybatis.harvey.sqlsession;

import com.mini.mybatis.harvey.sqlsession.configuration.Configuration;
import com.mini.mybatis.harvey.sqlsession.defaults.DefaultSqlSessionFactory;
import com.mini.mybatis.harvey.builder.xml.XmlConfigBuilder;

import java.io.Reader;

/**
 * MyBatis的入口
 */
public class SqlSessionFactoryBuilder {


    public SqlSessionFactory build(Reader reader){
        XmlConfigBuilder xmlConfigBuilder = new XmlConfigBuilder(reader);
        return build(xmlConfigBuilder.parse());
    }

    public SqlSessionFactory build(Configuration configuration){
        return new DefaultSqlSessionFactory(configuration);
    }


}
